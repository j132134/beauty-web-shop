require 'angular'
require 'angular-resource'
require 'angular-local-storage'
require 'angular-ui-router'
require 'angular-ui-router.stateHelper'

app = angular.module 'beauty-web-shop', [
  'ngResource'
  'LocalStorageModule'
  'ui.router'
  'ui.router.stateHelper'
]

app.config ($httpProvider, $compileProvider, localStorageServiceProvider) ->
  $httpProvider.defaults.useXDomain = true
  delete $httpProvider.defaults.headers.common['X-Requested-With']

  $httpProvider.defaults.headers.post['Content-Type'] = 'application/json'
  $httpProvider.defaults.headers.put['Content-Type'] = 'application/json'

  $compileProvider.debugInfoEnabled false

  localStorageServiceProvider.setPrefix 'hb'
  localStorageServiceProvider.setStorageCookie 0, '/'

app.init = ->
  angular.element(document).ready ->
    try
      angular.bootstrap document, ['beauty-web-shop']

      document.getElementById('screen').classList.add 'hidden'
      document.getElementById('indicator').classList.add 'hidden'

    catch e
      console.log e.stack or e.message or e


module.exports = app
