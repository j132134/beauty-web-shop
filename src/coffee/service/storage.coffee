app = require '../app'

app.service 'storage', (localStorageService) ->

  grid = localStorageService.get('grid') or {}

  @grid =
    get: (key) ->
      return grid[key]

    set: (key, value) ->
      grid[key] = value
      localStorageService.set 'grid', grid

    delete: (key) ->
      delete grid[key]
      localStorageService.set 'grid', grid

    remove: ->
      grid = {}
      localStorageService.remove 'grid'

  return
