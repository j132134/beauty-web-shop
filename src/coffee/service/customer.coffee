app = require '../app'

app.service 'customer', (shop, api) ->

  @getList = (callback) ->
    api.getCustomers
      params:
        shop_id: shop.current.id
      success: (res) ->
        return if not data = res.data
        callback data
  
  @getInfo = (customer_id, callback) ->
    api.getCustomer
      params:
        shop_id: shop.current.id
        customer_id: customer_id
      success: (res) ->
        return if not data = res.data
        callback data

  @getPurchasedPackages = (customer_id, callback) ->
    @getInfo customer_id, (data) ->
      callback data?.purchased_packages
  
  return
