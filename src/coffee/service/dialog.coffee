app = require '../app'

moment = require 'moment'

app.service 'dialog', ->

  @show = false
  @type = undefined
  @function = undefined
  @value = null
  @data = {}

  @preset =
    confirm: {width: '33rem', height: '13rem'}

  @open = (@type, @data) ->
    @show = true

  @close = ->
    @show = false
    @type = undefined
    @function = undefined
    @value = null
    @data = {}
    
  @keydown = (code) =>
    if alias = @data.alias?[code] then @data.callback[alias]?()
    if code is 27 then @close()

    if @type is 'edit_time'
      if code is 37 then @function.prev()
      if code is 39 then @function.next()
  return

app.directive 'hbDialog', ($http, $compile, dialog, customer, timeslot, shop) ->
  restrict: 'A'
  template: ->
    '<div class="box">' +
      '<div class="head"></div>' +
      '<div class="body"></div>' +
      '<div class="buttons"></div>' +
    '</div>'

  link: (scope, element) ->
    el =
      box: element.find '.box'
      head: element.find '.head'
      body: element.find '.body'
      buttons: element.find '.buttons'

    render =
      radio: ->
        container = $('<div class="options"></div>')
        container.appendTo el.body
        for key, value of dialog.data.option
          $('<input type="radio">')
          .attr 'id', "dialog-radio-#{key}"
          .attr 'name', 'select'
          .attr 'value', key
          .on 'change', -> dialog.value = @value
          .appendTo container
          $('<label></label>')
          .attr 'for', "dialog-radio-#{key}"
          .text value
          .appendTo container

      confirm: ->
        el.head.addClass 'message'
        
      edit_time: (ts = dialog.data.item.start_ts) ->
        el.head.hide()
        item = dialog.data.item
        moment_ts = if ts then moment ts, 'X' else moment()
        $http.get '/template/dialog/edit_time.html'
        .then (res) ->
          timeslot.getAvailable moment_ts.startOf('day').format('X'), item.service_duration, dialog.data.excludes, (data) ->
            dialog.function = _function =
              prev: -> render.edit_time moment_ts.subtract(1, 'd').format('X')
              next: -> render.edit_time moment_ts.add(1, 'd').format('X')
            scope.dialog =
              timeslots: _.reject data, hidden: true
              date: moment_ts.format 'MMM Do dddd'
              start_ts: dialog.data.item.start_ts
              day: _function
              select: (ts, index) ->
                dialog.value =
                  timeslots: data
                  data:
                    start_ts: ts
                    timeslot_id_list: _.slice _.map(@timeslots, 'id'), index, index + (item.service_duration / 30)
                dialog.data.callback()
            el.body.html $compile(res.data)(scope)

      common: ->
        el.box.removeAttr('style').css dialog.data.size
        el.head.show().removeClass().addClass 'head'
        el.head.html dialog.data.message
        el.body.empty()
        el.buttons.empty()
        for key, value of dialog.data.buttons
          $('<button></button>').text value
          .on 'click', bind.callback key
          .appendTo el.buttons

    bind =
      callback: (key) ->
        return ->
          dialog.data.callback[key]?() or dialog.close()
          scope.$apply()

    scope.$watch (-> dialog.show), (show) ->
      if show then element.show() else element.hide()
      return if not show
      render.common()
      render[dialog.type]?()

    el.box.on 'click', (e) -> e.stopPropagation()
    element.on 'click', ->
      el.box.addClass 'flicker'
      setTimeout (-> el.box.removeClass 'flicker'), 450
