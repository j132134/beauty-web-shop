app = require '../app'

moment = require 'moment'
validator = require '../utility/validator'
collection = require '../utility/collection'

app.service 'modal', ($rootScope, $state, $timeout, dialog, notify, customer, timeslot, shop, api) ->

  modelize = (item) ->
    item.keys = _.keys item if not item.keys
    start = moment item.start_ts, 'X'
    end = moment(start).add item.service_duration, 'm' if item.service_duration
    return _.assign item,
      if item.start_ts
        service_time: start.format('MMM Do HH:mm') + '~' + (end?.format('HH:mm') or '')
        isPast: moment().isAfter start
      else
        service_time: ''
        isPast: false

  normalize = (item) ->
    _.assign _.pick(item, item.keys),
      phone_num: item.phone_num?.replace(/\D/g, '') or null

  createReservation = (item, data, callback) =>
    @item = _.assign item, data
    api.createReservation
      params:
        shop_id: shop.current.id
      data:
        reservation: normalize @item
      success: (res) =>
        @item = modelize res.data
        $rootScope.$broadcast 'reservation:update'
        callback?()

  updateReservation = (item, data, callback) =>
    @item = _.assign item, data
    api.updateReservation
      params:
        shop_id: item.shop_id
        reservation_id: item.id
      data:
        reservation: normalize @item
      success: (res) =>
        @item = modelize res.data
        $rootScope.$broadcast 'reservation:update' if $state.is 'shop.reservation'
        callback?()
        
  do init = =>
    @isEdit = false
    @isNew = false
    @show = false
    @item = null
    @clone = null
    @data =
      packages: []
      services: []
      employees: []
      timeslots: []
    @value =
      package: null
      service: null
      employee: null

  @create = (ts) ->
    return
    @open {
      status: 'new'
      customer_id: null
      customer_name: ''
      customer_request: ''
      employment_id: null
      employee_name: ''
      package_id: null
      package_name: ''
      purchased_package_id: null
      service_id: null
      service_name: ''
      service_amount: ''
      service_duration: null
      timeslot_id_list: null
      start_ts: ts
    }
    @isNew = true
    @edit.enter()

  @open = (item) ->
    init()
    @show = true
    @item = modelize item

  @close = ->
    if not @isEdit or @isNew
      init()
      dialog.close() if dialog.show
    else
      dialog.open 'confirm',
        size: dialog.preset.confirm
        message: '수정하신 내용이 유실됩니다.\n 창을 닫으시겠습니까?'
        buttons: {no: '아니오', yes: '예'}
        alias: 13: 'yes'
        callback:
          yes: ->
            init()
            dialog.close()

  @keydown = (code) =>
    switch code
      when 27
        if @isEdit then @edit.cancel() else @close()
      when 13
        if @isEdit then @edit.save() else @edit.enter()

  @isCompleted = =>
    @item and _.includes ['completed', 'noshow', 'canceled'], @item.status

  @function =
    cancel: =>
      dialog.open 'confirm',
        size: dialog.preset.confirm
        message: '예악을 <b>취소</b>하시겠습니까?'
        buttons: {no: '아니오', yes: '예'}
        alias: 13: 'yes'
        callback:
          yes: => updateReservation @item, status: 'canceled', => @close()

    confirm: =>
      if @item.service_id
        updateReservation @item, status: 'confirmed'
      else
        notify.error '패키지 예약은 서비스를 선택하셔야 승인 가능합니다.'

    complete: =>
      dialog.open 'radio',
        size: {width: '30rem', height: if @item.customer_id then '17rem' else '15rem'}
        message: '결제 수단을 선택해주세요'
        option: if @item.customer_id then {cash: '현금', card: '카드', balance: '예치금'} else {cash: '현금', card: '카드'}
        buttons: {cancel: '취소', confirm: '확인'}
        alias: 13: 'confirm'
        callback:
          confirm: =>
            if not dialog.value
              notify.warn '결제 수단을 선택해주세요.'
              return
            updateReservation @item,
              status: 'completed'
              payment_method: dialog.value
            , => @close()

    noshow: =>
      dialog.open 'confirm',
        size: dialog.preset.confirm
        message: '예악 상태를 <b>노쇼</b>로 변경하시겠습니까?'
        buttons: {no: '아니오', yes: '예'}
        alias: 13: 'yes'
        callback:
          yes: => updateReservation @item, status: 'noshow', => @close()

  @edit =
    enter: =>
      return
      @isEdit = true
      @clone = _.assign _.clone(@item), edited_change_reason: ''
      @value =
        package: @clone.purchased_package_id?.toString()
        service: @clone.service_id?.toString()
        employee: @clone.employment_id?.toString()
      customer.getList (data) => @data.customers = data if @isNew
      @data.services = shop.data.services
      pull.packages() if @clone.customer_id
      pull.employees() if @clone.timeslot_id_list?.length
      pull.timeslots() if @clone.start_ts

    time: =>
      if not @clone.service_id
        notify.warn '서비스를 먼저 선택해주세요.'
        return
      dialog.open 'edit_time',
        size: {width: '27em', 'min-height': '16em'}
        item: @clone
        excludes: @item.timeslot_id_list
        buttons: {cancel: '취소'}
        callback: @edited.time

    cancel: =>
      if @isNew
        @close()
        return
      $timeout =>
        @isEdit = false
        @clone = null

    save: =>
      if message = validator.reservation @clone
        notify.error message
        return

      keys = ['purchased_package_id', 'service_id', 'employment_id', 'start_ts', 'change_reason']
      @clone.change_reason = @clone.edited_change_reason if @clone.edited_change_reason isnt ''
      if _.isEqual _.pick(@item, keys), _.pick(@clone, keys)
        notify.warn '수정된 사항이 없습니다.'
        @edit.cancel()

      saveReservation = if @isNew then createReservation else updateReservation
      saveReservation @item = @clone, status: 'confirmed', =>
        @clone = null
        @isEdit = false
        @isNew = false

  @edited =
    customer: (customer) =>
      @clone.customer_id = customer?.id or null
      @clone.customer_name = customer?.name or null
      @clone.phone_num = customer?.phone_num or null
      pull.packages reset: Boolean @clone.purchased_package_id

    package: =>
      @clone.service_id = @value.service = null

      _package = _.find @data.packages, id: Number @value.package
      @clone = _.assign @clone,
        package_id: _.package?.package_id or null
        package_name: _package?.package_name or null
        purchased_package: _package or null
        purchased_package_id: _package?.id or null

      if not @value.package
        @data.services = shop.data.services
      else
        selected = _.find @data.packages, id: Number @value.package
        if @clone.purchased_package_id isnt selected?.id
          @data.services = selected.unused_services
        else
          @data.services = collection.concat _.find(@clone.purchased_package.used_services, ['service_id', @clone.service_id]), @clone.purchased_package.unused_services

    service: =>
      if (_service = _.find shop.data.services, id: Number @value.service) and (start_ts = @clone.start_ts)
        timeslot.findIds
          slots: @data.timeslots
          start_ts: start_ts
          length: _service.duration / 30
          success: (data) =>
            @clone.timeslot_id_list = data
          error: =>
            notify.warn '서비스 시간을 다시 선택하세요.'
            @clone.timeslot_id_list = []
            @clone.start_ts = null
      @clone = modelize _.assign @clone,
        service_id: _service?.id or null
        service_name: _service?.name or null
        service_duration: _service?.duration or null
      pull.employees reset: true

    time: =>
      @data.timeslots = dialog.value.timeslots
      @clone = modelize _.assign @clone, dialog.value.data
      pull.employees reset: true
      dialog.close()

    employee: =>
      _employment = _.find @data.employees, id: Number @value.employee
      @clone = _.assign @clone,
        employment_id: _employment?.id or null
        employee_name: _employment?.employee_name or null

  pull =
    packages: (option) =>
      if option?.reset
        @clone.purchased_package = null
        @clone.purchased_package_id = @value.package = null
        @clone.service_id = @value.service = null
      if @clone.customer_id
        customer.getPurchasedPackages @clone.customer_id, (data) =>
          @data.packages = collection.concat @clone.purchased_package, _.reject(data, ['is_all_used', true])
          @data.services = collection.concat _.find(@clone.purchased_package.used_services, ['service_id', @clone.service_id]), @clone.purchased_package.unused_services if @clone.purchased_package_id
      else
        @data.packages = []
        @data.services = shop.data.services

    employees: (option) =>
      _id = @clone.employment_id
      if option?.reset
        @clone.employment_id = @value.employee = null
        @clone.employee_name = null
      if not @clone.timeslot_id_list?.length
        @data.employees = []
        return
      timeslot.getEmployees @clone.timeslot_id_list, (data) =>
        _isEqual = @clone.employment_id and _.isEqual _.pick(@clone, ['start_ts', 'service_duration']), _.pick(@item, ['start_ts', 'service_duration'])
        if _isEqual then _reserved = {id: @clone.employment_id, employee_name: @clone.employee_name}
        @data.employees = employees = collection.concat _reserved, _.filter data, available: true
        if _employment = _.find employees, ['id', _id]
          @value.employee = _employment.id.toString()
          @clone = _.assign @clone,
            employment_id: _employment.id
            employee_name: _employment.employee_name
            
    timeslots: =>
      ts = moment(@clone.start_ts, 'X').startOf('day').format('X')
      timeslot.getAvailable ts, @clone.service_duration, @item.timeslot_id_list, (data) =>
        @data.timeslots = data

  return
