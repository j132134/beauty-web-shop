app = require '../app'

moment = require 'moment'

app.service 'timeslot', (shop, api) ->

  modelize = (slot) ->
    _.assign slot,
      timeString: moment(slot.ts, 'X').format 'HH:mm'
      unavailable: false

  @getTimeslots = (ts, callback, isTomorrow = false) ->
    api.getTimeSlots
      params:
        shop_id: shop.current.id
        period: 'day'
        ts: ts
        short: true
      success: (res) =>
        return if not data = res.data
        m = moment _.last(data)?.ts, 'X'
        if not isTomorrow and m.hour() is 23 and m.minute() is 30 and _.last(data)?.available
          @getTimeslots moment(ts, 'X').add(1, 'd').format('X'), (tomorrow) ->
            slot.hidden = true for slot in tomorrow
            m = moment _.head(tomorrow)?.ts, 'X'
            data = _.concat data, tomorrow if m.hour() is 0 and m.minute() is 0
            callback data
          , true
        else
          callback data

  @getAvailable = (ts, duration, excludes, callback) ->
    @getTimeslots ts, (data) ->
      result = []
      repeat = duration / 30
      for slot in data
        if _.includes excludes, slot.id then slot.available++
      for slot, i in data
        modelize slot
        for j in [0...repeat]
          slot.unavailable = true if not data[i + j]?.available or not moment(data[i + j].ts, 'X').isSame moment(slot.ts, 'X').add(30 * j, 'm'), 'minute'
        result.push slot
      callback result

  @findIds = (data) ->
    index = _.findIndex data.slots, ts: data.start_ts
    result = _.slice data.slots, index, index + data.length
    for i in [0...data.length]
      if not result[i]?.available or not moment(result[i].ts, 'X').isSame moment(result[0].ts, 'X').add(30 * i, 'm'), 'minute'
        return data.error()
    data.success _.map result, 'id'

  @getEmployees = (ids, callback) ->
    api.getAvailableEmployees
      params:
        shop_id: shop.current.id
        'timeslot_ids[]': ids
      success: (res) ->
        return if not data = res.data
        callback data
  
  return
