app = require '../app'

app.factory 'apiSuccess', (notify) ->
  return (success, error) ->
    return (res) ->
      if res.success isnt false
        success? res
      else
        console.debug 'Error', JSON.stringify res
        notify.error error?.message or res?.message or '에러가 발생했습니다. 잠시 후 다시 시도해주세요.' if not error?.silent
        error?.callback? res

app.factory 'apiError', ($state, notify) ->
  return (e) ->
    return (res) ->
      console.debug 'Error', JSON.stringify res
      notify.error e?.message or res?.data?.message or '에러가 발생했습니다. 잠시 후 다시 시도해주세요.' if not e?.silent
      e?.callback? res

      if res.status is -1 and e?.required then $state.go 'api-error'

app.factory 'api', (rest, indicator, apiSuccess, apiError) ->
  
  ## app
  getMeta: (o) ->
    rest '/v2/merchant/app/meta_info'
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    
  ## auth
  smsAuth: (o) ->
    indicator.show()
    rest '/v2/merchant/app/sms_auth',
      params: o.params
    .post()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  validateToken: (o) ->
    indicator.show()
    rest '/employee_auth/validate_token',
      headers: o.headers
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  signin: (o) ->
    indicator.show(true)
    rest '/employee_auth/sign_in'
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide(true)

  signup: (o) ->
    indicator.show()
    rest '/v2/merchant/shops'
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  ## shops
  getShopInfo: (o) ->
    rest '/v2/merchant/shops/:shop_id',
      params: o.params
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  updateShopInfo: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id',
      params: o.params
    .put(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  # timeslot
  getTimeSlots: (o) ->
    indicator.show() if Boolean o.indicator
    rest '/v2/merchant/shops/:shop_id/timeslots',
      params: o.params
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide() if Boolean o.indicator

  updateTimeSlots: (o) ->
    rest '/v2/merchant/shops/:shop_id/timeslots',
      params: o.params
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  getAvailableEmployees: (o) ->
    rest '/v2/merchant/shops/:shop_id/timeslots/available',
      params: o.params
    .get(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  # reservation
  getReservations: (o) ->
    indicator.show() if Boolean o.indicator
    rest '/v2/merchant/shops/:shop_id/reservations',
      params: o.params
    , o.server
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide() if Boolean o.indicator

  createReservation: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/reservations',
      params: o.params
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  updateReservation: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/reservations/:reservation_id',
      params: o.params
    .put(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  addBlockTimes: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/block_times',
      params: o.params
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  updateBlockTimes: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/block_times/:block_id',
      params: o.params
    .put(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  removeBlockTimes: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/block_times/:block_id',
      params: o.params
    .delete()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  # customer
  getCustomers: (o) ->
    rest '/m1/shops/:shop_id/customers',
      params: o.params
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  getCustomer: (o) ->
    rest '/m1/shops/:shop_id/customers/:customer_id',
      params: o.params
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  # services
  getServices: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/services',
      params: o.params
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  createService: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/services',
      params: o.params
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  getService: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/services/:service_id',
      params: o.params
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  updateService: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/services/:service_id',
      params: o.params
    .put(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  deleteService: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/services/:service_id',
      params: o.params
    .delete()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  updateServiceOrder: (o) ->
    rest '/v2/merchant/shops/:shop_id/services/order',
      params: o.params
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  # employments
  getEmployments: (o) ->
    rest '/v2/merchant/shops/:shop_id/employments',
      params: o.params
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  addEmployment: (o) ->
    rest '/v2/merchant/shops/:shop_id/employments',
      params: o.params
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  getEmployment: (o) ->
    rest '/v2/merchant/shops/:shop_id/employments/:employment_id',
      params: o.params
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  updateEmployment: (o) ->
    rest '/v2/merchant/shops/:shop_id/employments/:employment_id',
      params: o.params
    .put(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  updateEmploymentSchedule: (o) ->
    rest '/v2/merchant/shops/:shop_id/employments/:employment_id/schedule',
      params: o.params
    .put(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)

  # portfolio
  getPortfolios: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/portfolios',
      params: o.params
    , 'mapi'
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  createPortfolio: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/portfolios',
      params: o.params
    , 'mapi'
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  updatePortfolio: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/portfolios/:id',
      params: o.params
    , 'mapi'
    .put(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  deletePortfolio: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/portfolios/:id',
      params: o.params
    , 'mapi'
    .delete()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  updatePortfolioOrder: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/portfolios/order',
      params: o.params
    , 'mapi'
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  requestPortfolio: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/portfolios/request_approval',
      params: o.params
    , 'mapi'
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  publishPortfolio: (o) ->
    indicator.show()
    rest '/v2/scv/shops/:shop_id/publish_portfolio',
      params: o.params
    , 'mapi'
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  scrapManagedPost: (o) ->
    indicator.show()
    rest '/v2/scv/shops/:shop_id/scrap_sns',
      params: o.params
    , 'mapi'
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  # post
  getPosts: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/posts',
      params: o.params
    , 'mapi'
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  createPost: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/posts',
      params: o.params
    , 'mapi'
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  updatePost: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/posts/:id',
      params: o.params
    , 'mapi'
    .put(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  searchPosts: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/posts/search',
      params: o.params
    , 'mapi'
    .get()
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()

  addPostBlacklist: (o) ->
    indicator.show()
    rest '/v2/merchant/shops/:shop_id/posts/blacklist',
      params: o.params
    , 'mapi'
    .post(o.data)
    .$promise.then apiSuccess(o.success, o.error), apiError(o.error)
    .then -> indicator.hide()
