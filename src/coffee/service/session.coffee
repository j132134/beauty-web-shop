app = require '../app'

greeting = require '../utility/greeting'

app.factory 'session', ($rootScope, $state, $timeout, localStorageService, setHeaders, api, notify, shop) ->

  return _this =

    validate: (options) ->
      options = _.defaults options, required: true

      if user = $rootScope.auth
        return user

      else if _.isObject auth = localStorageService.get 'auth'
        api.validateToken
          headers:
            'access-token': auth['access-token']
            'client': auth['client_id']
            'uid': auth['uid']
          success: (res) ->
            if res.data.shops.length
              _this.register user = res.data
              shop.setList user.shops
              notify.success greeting(user.name), 2500 if options.required
              return user
            else
              notify.warn '등록된 상점이 없습니다'
              _this.destroy()
          error:
            message: '사용자 인증에 실패했습니다. 다시 로그인 해주세요.'
            required: true
            silent: not options.required
            callback: (e) ->
              $state.transitionTo 'account.signin', null, location: 'replace'
              _this.destroy() if ~e.status
              return false

      else if options.required
        notify.warn if not $rootScope.updated then '로그인 후 이용 가능합니다.' else '인증이 만료되었습니다. 다시 로그인 해주세요.'
        $timeout -> $state.transitionTo 'account.signin', null, location: 'replace'
        return null

    register: (auth) ->
      $rootScope.auth = _.assign auth, localStorageService.get 'auth'
      setHeaders auth
      @save auth

    save: (auth) ->
      localStorageService.set 'auth', _.pick auth, ['uid', 'client_id', 'access-token']

    destroy: (o) ->
      _auth = Boolean $rootScope.auth
      $rootScope.auth = undefined
      localStorageService.remove 'auth'
      shop.current = undefined
      notify.info '정상적으로 로그아웃 되었습니다.' if _auth and o?.notify isnt false
      $state.go 'account.signin'
