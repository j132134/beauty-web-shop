app = require '../app'

app.factory 'notify', ($rootScope, $timeout) ->

  $rootScope.alerts = alerts =
    items: []
    index: 0

  remove = (id) ->
    $rootScope.alerts.items = _.reject alerts.items, id: id

  add = (data, duration) ->
    id = $rootScope.alerts.index++

    $rootScope.alerts.items.push
      id: id
      message: data.message
      type: data.type

    $timeout (-> remove id), duration or 1800

  return {
    success: (message, duration) -> add {type: 'success', message: message}, duration
    info: (message, duration) -> add {type: 'info', message: message}, duration
    warn: (message, duration) -> add {type: 'warn', message: message}, duration
    error: (message, duration) -> add {type: 'error', message: message}, duration
  }
