app = require '../app'

schedule = require '../utility/schedule'

app.service 'shop', ($rootScope, localStorageService, api) ->

  @current = {}

  @setList = (list) ->
    _last = @getDefault()
    if _last and _default = _.find list, ['shop_id', _last.id]
      @setDefault _default
    else
      @setDefault list?[0]

  @getDefault = ->
    return localStorageService.get 'shop'

  @setDefault = (target) ->
    return if not _.isObject target
    @current = $rootScope.shop =
      id: target.shop_id
      name: target.shop_name
      employment_id: target.id
      role: target.role
    localStorageService.set 'shop', @current

  @setData = (@data) ->
    # categories
    _.chain(@data.category_id_list ?= @data.category_ids).uniq().reject(_.isNull).value()
    # service cagegories
    @data.service_categories = _.flatMap @data.category_id_list, (shop_category_id) =>
      _.chain(@data.service_categories).filter(category_id: shop_category_id).orderBy('priority').value()
    @data.service_categories.push {id: null, category_id: null, name: '미분류', priority: 1000}
    # services
    @data.services = _.map @data.services, (item) => _.assign _.pick(item, ['id', 'service_name', 'duration', 'sort_order']),
      category_id: item.service_category_id
      category_name: (category = _.find @data.service_categories, id: item.service_category_id)?.name or '미분류'
      category_priority: if category then category.priority else 1000
    # employments
    @data.employments ?= []
    _.each @data.employments, (employee) -> employee.schedules = schedule.modelize employee.data = employee.schedules
    @data.employments = _.sortBy @data.employments, (e) -> e.employee_name.toLowerCase()
    @data.employments = (_.remove @data.employments, id: @current.employment_id).concat @data.employments if @current.employment_id

  @getInfo = (user) ->
    api.getShopInfo
      params:
        shop_id: @current.id
      success: (res) =>
        $rootScope.init_ts = res.meta
        @setData res.data
        return @data
      error:
        required: true
        silent: not user

  @updateInfo = (data, callback) ->
    api.updateShopInfo
      params:
        shop_id: @current.id
      data: _.chain(data).omit(['services', 'employments', 'schedule']).assign({category_ids: data.category_id_list}).value()
      success: (res) =>
        @setData res.data
        callback? @data

  return
