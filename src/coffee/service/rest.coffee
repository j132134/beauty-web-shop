app = require '../app'

app.service 'server', (localStorageService) ->
  live = 'https://api.heybeauty.kr'
  ls = localStorageService.get 'server'
  if _.isObject ls
    @name = ls.name
    @url = ls.url
  else if _.isString ls
    @name = 'custom'
    @url = ls
  else
    @name = null
    @url = live
  return

app.factory 'setHeaders', ($rootScope) ->
  return (auth) ->
    $rootScope.headers =
      'access-token': auth['access-token']
      'client': auth['client_id']
      'uid': auth['uid']

app.factory 'rest', ($rootScope, $resource, server) ->
  return (endpoint, o, dest) ->
    params = o?.params or {}
    headers = o?.headers or {}

    _.assign headers, $rootScope.headers

    switch dest
      when 'api'
        url = 'https://api.heybeauty.kr'
      when 'mapi'
        url = 'https://mapi.heybeauty.kr'
      else
        url = server.url

    $resource url + endpoint, null,
      get:
        method: 'GET'
        params: params
        headers: headers
      query:
        method: 'GET'
        params: params
        headers: headers
        isArray: true
      post:
        method: 'POST'
        params: params
        headers: headers
      put:
        method: 'PUT'
        params: params
        headers: headers
      delete:
        method: 'DELETE'
        params: params
        headers: headers
