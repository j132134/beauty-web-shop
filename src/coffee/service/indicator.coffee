app = require '../app'

app.service 'indicator', ->
  @count = 0
  @screenCnt = 0

  @show = (screen = false) ->
    @count++
    @screenCnt++ if screen

  @hide = (screen = false) ->
    @count-- if @count isnt 0
    @screenCnt-- if screen

  return

app.directive 'hbIndicator', (indicator) ->
  restrict: 'A'
  link: (scope, element) ->
    scope.$watch (-> indicator.count), (value) ->
      if not value
        element.fadeOut(100)
      else
        element.show()

app.directive 'hbScreen', ($timeout, indicator) ->
  restrict: 'A'
  link: (scope, element) ->
    scope.$watch (-> indicator.screenCnt), (value) ->
      if not value
        $timeout (-> element.fadeOut(450)), 200
      else
        element.fadeIn(150)
