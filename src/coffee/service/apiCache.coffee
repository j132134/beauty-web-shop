app = require '../app'

schedule = require '../utility/schedule'

app.factory 'apiCache', (api) ->

  cache = {}

  return {
    reset: ->
      cache = _.pick cache, 'meta'

    getMeta: ->
      cache.meta or api.getMeta
        success: (res) ->
          cache.meta = res.data

    getEmployments: (params) ->
      cache.employments or api.getEmployments
        params: params
        success: (res) ->
          cache.employments = _.chain res.data
          .each (item) -> item.schedules = schedule.modelize item.employment_schedules
          .orderBy ['role', 'status', 'employee_name'], ['desc', 'asc', 'asc']
          .value()
  }