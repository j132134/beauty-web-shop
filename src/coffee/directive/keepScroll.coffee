app = require '../app'

app.directive 'hbKeepScroll', ($state, localStorageService) ->
  scope:
    selector: '@scrollTo'

  link: (scope, element) ->

    scope.$on '$stateChangeStart', (e, toState) ->
      if toState.name is $state.current.name
        localStorageService.set 'keepScroll', element.scrollTop()
      else
        localStorageService.remove 'keepScroll'

    scope.$on '$stateChangeSuccess', ->
      return if not last = localStorageService.get 'keepScroll'
      placeholder = $('<div></div>').appendTo element
      placeholder.height last + element.outerHeight()
      element.scrollTop last

      scope.$evalAsync -> placeholder.remove()

      scope.$applyAsync ->
        return if not (selected = element.children scope.selector).length
        to = selected.offset().top - element.offset().top + element.scrollTop()
        min = localStorageService.get('keepScroll') or 0
        max = min + element.outerHeight()
        if to < min or to > max then element.scrollTop to - 30