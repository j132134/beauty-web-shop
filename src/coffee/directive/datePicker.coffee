app = require '../app'

moment = require 'moment'

app.directive 'hbDatePicker', ($parse, $document) ->
  require: '?ngModel'
  scope: {}
  replace: true
  templateUrl: '/components/datePicker.html'
  link: (scope, element, attrs, ngModel) ->
    format = attrs.hbDatePicker
    watcher = attrs.hbDatePickerOpen

    scope.opened = false
    scope.days = []
    scope.dates = ['일', '월', '화', '수', '목', '금', '토']
    scope.viewValue = null
    scope.dateValue = null

    calendar =
      open: ->
        @generate()
        scope.opened = true
      close: ->
        scope.opened = false
      generate: (date = scope.viewValue) ->
        scope.date = date = moment(date)
        year = date.year()
        month = date.month() + 1
        today = moment().year() is year and moment().month() + 1 is month and moment().date()
        firstDay = date.clone().startOf 'month'
        lastDay = date.clone().endOf 'month'
        scope.month = date.format 'YYYY[년] MM[월]'
        scope.days = []
        for i in [0...firstDay.days()]
          scope.days.push {date: null, month: null, year: null}
        for i in [firstDay.date()..lastDay.date()]
          scope.days.push {date: i, month: month, year: year, today: i is today}

    scope.now = -> calendar.generate moment()
    scope.prev = (unit) -> calendar.generate scope.date.subtract 1, unit
    scope.next = (unit) -> calendar.generate scope.date.add 1, unit

    scope.setDate = (day) ->
      return if not day.date
      ngModel.$setViewValue scope.viewValue = moment("#{day.year}-#{day.month}-#{day.date}", 'YYYY-M-D').format format
      ngModel.$modelValue = scope.viewValue
      calendar.close()

    ngModel.$render = ->
      scope.viewValue = ngModel.$viewValue

    input = element.find 'input'
    input.on 'focus', ->
      input.blur()
      calendar.open()
      scope.$apply()
    input.on 'keydown', (e) ->
      e.preventDefault()

    $document.on 'click', (e) ->
      return if not scope.opened
      calendar.close()
      scope.$apply()

    element.on 'click', (e) ->
      e.stopPropagation()

    if watcher then scope.$parent.$watch watcher, (value) ->
      return if not value
      scope.$applyAsync -> calendar.open()
      $parse(watcher).assign scope.$parent, false
