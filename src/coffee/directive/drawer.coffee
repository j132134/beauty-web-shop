app = require '../app'

state = require '../utility/state'

app.directive 'hbDrawer', ($rootScope, $state, $window, $timeout) ->
  link: (scope, element) ->
    shop = element.find '#shop'
    inner = element.find '.aside'
    items = element.find 'li[data-sref]'
    
    scope.drawer =
      init: ->
        _.each items, (item) ->
          $(item).children().eq(0).text state.getTitle $(item).attr('data-sref').replace /\(.*\)$/, ''
      update: (name) ->
        items.removeClass 'active'
        item = _.filter items, (item) -> name is $(item).attr('data-sref').replace /\(.*\)$/, ''
        if item.length > 0
          $(item[0]).addClass 'active'
        else
          item = _.filter items, (item) -> name.includes $(item).attr('data-sref').replace /\(.*\)$/, ''
          $(item[0]).addClass 'active'

    $rootScope.drawer = drawer =
      isOpen: false
      open: ->
        @isOpen = true
        element.addClass 'open'
        $timeout (-> inner.addClass 'open'), 50
      close: ->
        @isOpen = false
        inner.removeClass 'open'
        $timeout (-> element.removeClass 'open'), 150
        list.close()

    list =
      expand: false
      open: ->
        @expand = true
        shop.addClass 'expanded'
      close: ->
        @expand = false
        shop.removeClass 'expanded'
      toggle: ->
        @expand = not @expand
        shop.toggleClass 'expanded'

    element.on 'click', '.toggle', ->
      drawer.open()

    element.on 'click', 'li', (e) ->
      target = $(e.target).closest 'li'
      shop_id = target.attr 'data-id'
      toState = (sref = target.attr 'data-sref') and (_ref = /([^(]+)(?:\((.+)\))?/.exec sref)[1]
      params = scope.$eval _ref?[2] or '{}'
      $timeout ->
        if shop_id
          scope.shop.switching _.find $rootScope.auth.shops, shop_id: Number shop_id
        else if toState
          options = location: 'replace' if $state.is toState
          $state.go toState, params, _.assign options, inherit: false
      , if drawer.isOpen then 150 else 0
      drawer.close()

    element.on 'click', 'h2', ->
      list.toggle()

    element.on 'click', (e) ->
      drawer.close() if e.target.id is 'menu'
