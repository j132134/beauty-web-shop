app = require '../app'

app.directive 'hbSelectBox', ->
  restrict: 'E'

  scope:
    model: '=hbSelectModel'
    items: '=hbSelectItems'
    key: '@hbSelectKey'
    label: '@hbSelectLabel'
    order: '@hbSelectOrder'

  controller: ($scope) ->
    $scope.select = undefined
    $scope.selected = []
    $scope.unselected = []

    $scope.$watch 'select', (val) ->
      return if not val
      $scope.selected.push _.remove($scope.unselected, [$scope.key, val])[0]
      $scope.update()

    $scope.remove = (event, val) ->
      $scope.unselected.push _.remove($scope.selected, [$scope.key, val])[0]
      $scope.unselected = _.sortBy $scope.unselected, $scope.order
      $scope.update()

    $scope.update = ->
      $scope.model = _.map $scope.selected, $scope.key

  compile: ->
    pre: (scope) ->
      watcher = scope.$watch 'items', (items) ->
        return if not scope.unselected = angular.copy items
        _.chain(scope.model).clone().map (val) ->
          scope.selected.push _.remove(scope.unselected, [scope.key, val])[0]
        .value()
        watcher()

  template: ->
    '<div>' +
      '<ul>' +
        '<li ng-repeat="item in selected track by item.id" class="item remove">' +
          '{{item[label]}} <button type="button" ng-click="remove($event, item[key])">지우기</button>' +
        '</li>' +
        '<div class="item add" ng-show="unselected.length">' +
          '추가 <select ng-model="select" ng-options="c[key] as c[label] for c in unselected | orderBy:\'{{order}}\'"></select>' +
        '</div>' +
      '</ul>' +
    '</div>'

  replace: true
