app = require '../app'

app.directive 'svgTextEllipsis', ->
  link: (scope, element, attrs) -> scope.$applyAsync ->
    _element = element[0]
    return if _element.getComputedTextLength() < width = attrs.svgTextEllipsis
    remainings = _element.textContent.split ''
    _element.textContent = ''

    while remainings.length
      _element.textContent += remainings.shift()
      if _element.getComputedTextLength() > width - 6
        _element.textContent += '..'
        break

app.directive 'svgTextWrap', ->
  link: (scope, element, attrs) -> scope.$applyAsync ->
    lineClamp = Math.floor attrs.svgLineClamp
    return if lineClamp and element[0].getComputedTextLength() < width = attrs.svgTextWrap
    remainings = element[0].textContent.split ''
    tspans = ['']
    i = 0

    element.empty()
    tspan = document.createElementNS 'http://www.w3.org/2000/svg', 'tspan'
    $(tspan).appendTo element

    while remainings.length
      tspan.textContent = tspans[i] += r = remainings.shift()
      if tspan.getComputedTextLength() > width
        tspans[i] = tspans[i].slice 0, tspans[i].length - 1
        tspans[++i] = r

    $(tspan).remove()
    _.each tspans.slice(0, lineClamp), (text, i) ->
      tspan.textContent = text.trim()
      tspan.setAttributeNS null, 'x', attrs.x
      tspan.setAttributeNS null, 'y', attrs.y
      tspan.setAttributeNS null, 'dx', attrs.dx
      tspan.setAttributeNS null, 'dy', Number(attrs.dy) + 18 * i
      $(tspan).clone().appendTo element
