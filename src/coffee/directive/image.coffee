app = require '../app'

app.directive 'hbImageBase64', ->
  scope:
    callback: '&'

  link: (scope, element, attrs) ->
    size = attrs.hbImageBase64 isnt 'hb-image-base64' and attrs.hbImageBase64.split ','
    cw = Number size?[0]
    ch = Number size?[1]

    canvas = $('<canvas></canvas>')
    if cw then canvas.attr 'width', cw
    if ch then canvas.attr 'height', ch

    ratio = cw / ch if size
    input = element.find 'input[type=file]'
    context = canvas[0].getContext '2d'

    element.on 'click', -> input.click()
    input.on 'click', (e) -> e.stopPropagation()

    input.on 'change', ->
      return if not @files?[0]
      render = new FileReader()
      render.onload = (e) ->
        image = new Image()
        image.onload = ->
          width = image.width
          height = image.height
          if not size
            canvas.attr 'width', width
            canvas.attr 'height', height
          else if (width / height) > ratio
            width = height * ratio
            sx = (image.width - width) / 2
          else if (width / height) < ratio
            height = width / ratio
            sy = (image.height - height) / 2
          context.drawImage image, sx or 0, sy or 0, width, height, 0, 0, cw or width, ch or height
          base64 = canvas[0].toDataURL 'image/jpeg', quality = 1
          base64 = canvas[0].toDataURL 'image/jpeg', quality -= .05 while base64.length > 1398101
          scope.callback data: base64
          scope.$apply()
          context.clearRect 0, 0, cw or width, ch or height
          input.val ''
        image.src = e.target.result
      render.readAsDataURL @files[0]

