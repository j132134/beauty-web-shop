app = require '../app'

app.directive 'hbSortable', ->
  scope:
    callback: '&hbSortable'

  compile: ($element) ->
    ngRepeat = $element.children '[ng-repeat]'
    ngRepeatExp = ngRepeat.attr 'ng-repeat'
    tagName = ngRepeat.get(0).tagName.toLowerCase()

    $element.css position: 'relative'
    usePointer = Boolean $element.find('[hb-sortable-pointer]')[0]

    match = ngRepeatExp.match /^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+track\s+by\s+([\s\S]+?))?\s*$/
    # lhs = match[1]
    rhs = match[2]
    # match = lhs?.match /^(?:(\s*[\$\w]+)|\(\s*([\$\w]+)\s*,\s*([\$\w]+)\s*\))$/
    # value = match[3] or match[1]
    # key = match[2]

    return ($scope, $element) ->
      children = undefined
      list = undefined
      ids = undefined
      $scope.$parent.$watchCollection rhs, (collection) ->
        return if not collection
        children = $element.children tagName
        list = collection
        ids = _.map list, 'id'

      clicked = false
      target = undefined
      offset = undefined
      tOffset = undefined
      clone = undefined
      start = {}
      maps = []

      touchstart = (e) ->
        return if usePointer and not $(e.target).is '[hb-sortable-pointer]'
        e.preventDefault()

        clicked = true
        target = $(e.target).closest tagName
        offset = $element.offset()
        tOffset = target.offset()
        clone = target.clone()

        target.addClass 'selected'
        clone.addClass('clone').appendTo $element

        start =
          x: e.pageX or e.originalEvent.touches?[0].pageX
          y: e.pageY or e.originalEvent.touches?[0].pageY
          index: Number target.css 'order'
          ts: Date.now()

        clone.css
          position: 'absolute'
          top: start.y - offset.top
          left: start.x - offset.left
          transform: "translate(#{tOffset.left - start.x}px, #{tOffset.top - start.y}px)"

        maps = do -> _.map children, (child) ->
          _offset = $(child).offset()
          return {
            x1: Number _offset.left
            x2: Number(_offset.left) + Number $(child).outerWidth()
            y1: Number _offset.top
            y2: Number(_offset.top) + Number $(child).outerHeight()
          }

      touchmove = (e) ->
        return if not clicked
        e.preventDefault()

        pageX = e.pageX or e.originalEvent.touches?[0].pageX
        pageY = e.pageY or e.originalEvent.touches?[0].pageY

        clone.css
          top: pageY - offset.top
          left: pageX - offset.left

        index = _.findIndex maps, (map) -> map.x1 < pageX < map.x2 and map.y1 < pageY < map.y2
        if ~index
          $scope.changes = index
          $scope.$apply()

      $scope.$watch 'changes', (to) ->
        return if to is undefined
        target.css 'order', to
        _.each [to...start.index], (i) ->
          dest = $(children[i])
          dest.css 'order', Number(dest.css 'order') + if to > start.index then -1 else 1
        children = _.orderBy children, (child) -> Number $(child).css 'order'
        start.index = to

      touchend = (e) ->
        return if not clicked
        e.preventDefault()
        clicked = false
        clone.remove()
        target.removeClass 'selected'

        order = _.map children, (child) -> Number $(child).attr 'data-id'
        if not _.isEqual order, ids
          $scope.callback ids: ids = order
        else if Date.now() - start.ts < 300
          target.click()

      $element.on 'touchstart mousedown', tagName, touchstart
      $('body').on 'touchmove', _.debounce touchmove, 10
      $(document).on 'mousemove', _.debounce touchmove, 10
      $('body').on 'touchend', touchend
      $(document).on 'mouseup', touchend
