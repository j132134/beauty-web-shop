app = require '../app'

moment = require 'moment'
time = require '../utility/time'

app.directive 'hbGrid', ($rootScope, $state, $stateParams, $location, $interval, storage) ->
  scope: false
  link: (scope, element) ->
    scope.posX = posX = if scope.personal then 0 else storage.grid.get('scrollLeft') or 0
    scope.posX = posX = Math.max posX, scope.maxScroll if posX < 0

    scope.posY = posY = 0
    header = height: $('#header').height()
    scope.root = root = height: scope.wrapper.height
    max = (root.height - scope.weeks.height) - scope.times.height * 48
    board =
      times: element.find '.times'
      reservation: element.find '.reservation'

    clicked = false
    selected = false
    beforeSelect = false
    drag = false
    moved = 0
    paused = false
    starts = {}
    startPos = {}
    offset = element.parent().offset()
    highlight =
      columns: element.find '.columns .highlight'
      times: element.find '.times .highlight'
      cell: element.find '.reservation .highlight'

    reset = (index) ->
      highlight.times.attr 'height', scope.times.height
      highlight.cell.attr 'height', scope.times.height
      return if not index
      highlight.cell.removeClass('selected')
      highlight.columns.removeClass('hidden').attr 'x', scope.xPos[index.x]
      highlight.times.removeClass('hidden').attr 'y', scope.weeks.height + scope.times.height * index.y
      highlight.cell.removeClass('hidden').attr 'x', scope.xPos[index.x]
      .attr 'y', scope.weeks.height + scope.times.height * index.y

    hide = (cell = false) ->
      highlight.cell.addClass 'hidden'
      return if cell
      highlight.times.addClass 'hidden'
      highlight.columns.addClass 'hidden'

    scope.release = release = (option) ->
      option = _.defaults option, {apply: false, ready: false}
      selected = false if not option.ready
      functions.hide option.apply
      reset()
      hide()

    ## drag
    _drag =
      start: (e) ->
        return if e.which isnt 1 or not $(e.target).closest('svg')[0]
        selected = true if scope.functions.show
        release {ready: true, apply: true}

        startPos = position =
          x: e.pageX - offset.left - scope.posX
          y: e.pageY - offset.top - scope.weeks.height
        starts =
          x: _.sortedIndex(scope.xPos, position.x) - 1
          y: parseInt((position.y - posY) / scope.times.height)

        return if starts.x < 0 or Boolean scope.matrix[starts.x][starts.y] or position.x < scope.times.width or position.y < 0

        beforeSelect = selected
        selected = false
        clicked = true
        drag = false
        moved = 0

        reset starts

      move: (e) ->
        return if selected

        position =
          x: e.pageX - offset.left - scope.posX
          y: e.pageY - offset.top - scope.weeks.height
        indexes =
          x: _.sortedIndex(scope.xPos, position.x) - 1
          y: parseInt((position.y - posY) / scope.times.height)

        if not clicked
          reset indexes if not e.which
          if position.x < scope.times.width or position.y < 0 then hide()
          if Boolean scope.matrix[indexes.x]?[indexes.y] then hide true

        else
          if indexes.y < paused then paused = false
          return if paused or scope.matrix[starts.x][indexes.y] and indexes.y > starts.y and paused = indexes.y

          moved = Math.max 0, indexes.y - starts.y
          if moved then drag = true

          highlight.times.attr 'height', scope.times.height * (moved + 1)
          highlight.cell.attr 'height', scope.times.height * (moved + 1)

      end: (e) ->
        return if not $(e.target).closest('svg')[0]
        if selected then release()
        return if e.which isnt 1 or not clicked

        paused = false
        clicked = false
        if beforeSelect
          release() if not selected = drag
        else
          selected = true

        highlight.cell.toggleClass 'selected', selected
        functions.show convertPos x: e.pageX, y: e.pageY if selected

    element.on 'mousedown', _drag.start
    element.on 'mousemove', _.throttle _drag.move, 20
    $(document).on 'mouseup', _drag.end

    element.on 'mouseleave', ->
      return if selected or clicked
      reset()
      hide()

    scope.convertPos = convertPos = (pos) ->
      position = {}
      pageX = Math.min Math.max(pos.x, min = scope.xPos[starts.x] + offset.left + scope.posX), min + scope.xWidth
      pageY = Math.min pos.y, offset.top + scope.weeks.height + scope.times.height * (starts.y + moved + 1) + posY, scope.wrapper.height + offset.top
      _.assign position, if (pageX - offset.left) / scope.wrapper.width < .8 then left: "#{pageX - offset.left + 5}px" else right: "#{scope.wrapper.width + offset.left - pageX + 5}px"
      _.assign position, if (pageY - offset.top) / scope.wrapper.height < .85 then top: "#{pageY + 5}px" else bottom: "#{scope.wrapper.height + offset.top - pageY + 5}px"
      return position

    ## functions
    functions =
      show: (position) ->
        _start = time.parse starts.y / 2
        _end = time.parse (starts.y + moved + 1) / 2
        date = moment scope.start
        date.weekday starts.x if scope.personal
        start = moment(date).hour(_start.hour).minute(_start.minute).startOf 'minute'
        end = moment(date).hour(_end.hour).minute(_end.minute).startOf 'minute'

        scope.functions.position = position
        scope.functions.selected =
          employment_id: if scope.personal then scope.employment.id else scope.employments[starts.x]?.id
          date: date.format 'M[/]DD[ ]dd'
          start_ts: start.format 'X'
          end_ts: end.format 'X'

        scope.functions.type = 'normal'
        scope.functions.show = true
        scope.$apply()
      hide: (apply = true) ->
        scope.functions.reset()
        scope.$apply() if apply

    ## scroll
    element.on 'mousewheel DOMMouseScroll MozMousePixelScroll', (event) ->
      originalEvent = event.originalEvent
      return if Math.abs(originalEvent.wheelDeltaX or 0) > Math.abs(originalEvent.wheelDeltaY or 0)
      event.preventDefault()
      scope.posY = posY += originalEvent.wheelDelta or -originalEvent.detail
      setBoard posY
      release(apply: true) if not clicked

    setBoard = (pos) ->
      scope.posY = posY = Math.max Math.min(pos, 0), max
      storage.grid.set 'scrollTop', posY
      board.times.attr 'transform', "translate(0, #{posY})"
      board.reservation.attr 'transform', "translate(#{scope.posX or 0}, #{posY})"

    do scope.scrollBoard = ->
      if _.isUndefined scope.posY = posY = storage.grid.get 'scrollTop'
        now = moment()
        top = (now.hour() + now.minute() / 60) / 24
        scope.posY = posY = 0 - scope.times.height * 48 * (top - 0.05)
      setBoard posY

    ## keyboard bind
    scope.$on 'keydown', (e, args) ->
      switch args.code
        when 37 # left
          scope.setDate.prev() if not scope.functions.show
        when 39 # right
          scope.setDate.next() if not scope.functions.show
        when 38 # up
          setBoard posY + scope.times.height if not scope.functions.show
        when 40 # down
          setBoard posY - scope.times.height if not scope.functions.show
        when 27 # esc
          scope.setDate.today() if not scope.functions.show
          release()