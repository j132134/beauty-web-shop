app = require '../app'

parser = require '../utility/parser'

app.directive 'hbBlockEnter', ->
  scope: false
  link: (scope, element) ->
    element.on 'keydown', (e) ->
      if e.keyCode is 13
        e.preventDefault()
        e.stopPropagation()

app.directive 'hbFormat', ->
  scope:
    value: '=ngModel'
    data: '=ngBind'
    type: '@hbFormat'
  link: (scope, element, attrs) ->
    if not element.is 'input'
      scope.data = parser[scope.type] scope.data or ''
    else
      skip = false
      watcher = scope.$watch 'value', (value, old) ->
        return if not value

        if skip
          skip = false
        else if _value = parser[scope.type] value, old, element[0].selectionStart
          skip = _.isObject _value
          scope.value = if _.isObject(_value) then _value.string else _value

      element.on '$destroy', -> watcher()

      if attrs.type is 'tel' or attrs.type is 'number' then element.on 'keypress', (e) ->
        return if e.metaKey or e.ctrlKey or e.altKey or e.which is 8
        e.preventDefault() if _.isNaN Number String.fromCharCode e.keyCode or e.which

app.directive 'hbValidator', ->
  scope: true
  link: (scope, element, attrs) ->
    regex = new RegExp attrs.hbValidator
    element.on 'blur', ->
      if element.val() and not result = regex.exec element.val()
        element.attr 'not-valid', ''
      else
        element.val result[1] if result?[1]
        element.removeAttr 'not-valid'

app.directive 'hbKeyup', ->
  scope:
    callback: '&hbKeyup'
  link: (scope, element) ->
    element.on 'keyup', (e) ->
      if e.keyCode is 27 then element.val ''
      scope.callback keyword: element.val()
      scope.$apply()

app.directive 'hbAutoIndent', ->
  scope: true
  link: (scope, element, attrs) ->
    property = attrs.property or 'padding-left'
    element.css property, parseInt(element.css property) + element.next(attrs.hbAuthIndent).width()
