app = require '../app'

moment = require 'moment'
time = require '../utility/time'

app.directive 'hbGridItem', (shop, api) ->
  scope: true
  template: ($element, $attrs) ->
    switch $attrs.hbGridItem
      when 'reservation'
        '<rect class="box" ng-class="data.status" ng-attr-x="{{attrs.x}}" ng-attr-y="{{attrs.y}}" ng-attr-width="{{attrs.width}}" ng-attr-height="{{attrs.height}}"></rect>' +
        '<text ng-bind="data.customer_name" ng-attr-x="{{attrs.x}}" ng-attr-y="{{attrs.y}}" dx="10" dy="10" svg-text-ellipsis="{{attrs.width-26}}"></text>' +
        '<text ng-bind="data.service_name" ng-attr-x="{{attrs.x}}" ng-attr-y="{{attrs.y}}" dx="10" dy="30" svg-text-wrap="{{attrs.width-20}}" svg-line-clamp="{{attrs.height/20-2}}"></text>' +
        '<rect class="pointer bottom" ng-attr-x="{{attrs.x}}" ng-attr-y="{{attrs.y+attrs.height-13}}" ng-attr-width="{{attrs.width}}" height="13"></rect>' +
        '<use class="pointer bottom" ng-attr-x="{{attrs.x+attrs.width/2-14}}" ng-attr-y="{{attrs.y+attrs.height-8}}" width="28" height="4" xlink:href="/img/grid.svg#pointer" ng-show="editable" />'
      else
        '<rect class="box" ng-attr-x="{{attrs.x}}" ng-attr-y="{{attrs.y}}" ng-attr-width="{{attrs.width}}" ng-attr-height="{{attrs.height}}"></rect>' +
        '<use class="block" ng-attr-x="{{attrs.x+attrs.width-25}}" ng-attr-y="{{attrs.y+10}}" width="15" height="20" xlink:href="/img/grid.svg#block" />' +
        '<rect class="pointer top" ng-attr-x="{{attrs.x}}" ng-attr-y="{{attrs.y}}" ng-attr-width="{{attrs.width}}" height="11" ng-show="editable"></rect>' +
        '<use class="pointer top" ng-attr-x="{{attrs.x+attrs.width/2-14}}" ng-attr-y="{{attrs.y+5}}" width="28" height="4" xlink:href="/img/grid.svg#pointer" ng-show="editable" />' +
        '<rect class="pointer bottom" ng-attr-x="{{attrs.x}}" ng-attr-y="{{attrs.y+attrs.height-13}}" ng-attr-width="{{attrs.width}}" height="13"></rect>' +
        '<use class="pointer bottom" ng-attr-x="{{attrs.x+attrs.width/2-14}}" ng-attr-y="{{attrs.y+attrs.height-8}}" width="28" height="4" xlink:href="/img/grid.svg#pointer" ng-show="editable" />'

  controller: ($scope, $element, $attrs) -> $scope.$evalAsync ->
    _scope = $scope.$parent
    
    list = if $attrs.hbGridItem is 'reservation' then _scope.reservations else _scope.blocks
    $scope.index = index = _.findIndex list, id: Number $attrs.id
    $scope.data = data = list[index]

    $scope.padding = padding = if $attrs.hbGridItem is 'block' then 2 else 1

    $scope.attrs = attrs =
      x: _scope.xPos[$attrs.x] + 3
      y: _scope.weeks.height + 2 * _scope.times.height * $attrs.y1 + padding
      width: _scope.xWidth - 6
      height: 2 * _scope.times.height * ($attrs.y2 - $attrs.y1) - 2 * padding

    if (count = data.extended?.total) > 1 then _.assign attrs,
      width: width = (_scope.xWidth - 6 - 2 * (count - 1)) / count
      x: _scope.xPos[$attrs.x] + 3 + (width + 2) * data.extended.degree

  link: (scope, element, attrs) ->
    _scope = scope.$parent
    functions = _scope.functions

    element.on 'click', 'use.block', (e) ->
      return if attrs.hbGridItem isnt 'block'
      e.preventDefault()
      functions.selected = id: attrs.id
      functions.position = _scope.convertPos x: e.pageX, y: e.pageY
      functions.type = 'unlock'
      functions.show = true
      functions.callback = ->
        unlockMatrix()
        element.remove()
        _scope.blocks[scope.index] = null
      scope.$apply()

    element.on 'mouseover', ->
      return if _scope.itemDrag
      scope.editable = true
      scope.$apply()

    element.on 'mouseleave', ->
      return if clicked
      scope.editable = false
      scope.$apply()

    ## drag
    clicked = false
    start = null
    moved = 0
    drag = 0

    _drag =
      start: (e) ->
        e.stopPropagation()
        $(document).on 'mousemove', throttled
        $(document).one 'mouseup', _drag.end

        _scope.release apply: true
        start = e.pageY - _scope.posY
        clicked = if $(e.target).hasClass 'top' then 'top' else 'bottom'
        _scope.itemDrag = true

      move: (e) ->
        return if not clicked
        top = clicked is 'top'
        direction = if top then -1 else 1
        moved = e.pageY - _scope.posY - start
        drag = moved / _scope.times.height / 2
        drag = if top then Math.min(drag, attrs.y2 - attrs.y1 - 0.5) else Math.max(drag, attrs.y1 - attrs.y2 + 0.5)
        scope.attrs.height = Math.round 2 * _scope.times.height * (attrs.y2 - attrs.y1 + drag * direction) - 2 * scope.padding
        scope.attrs.y = _scope.weeks.height + 2 * _scope.times.height * (Number(attrs.y1) + drag) + scope.padding if top
        scope.$apply()

      end: ->
        if clicked is 'bottom'
          _time = time.parse Number(attrs.y2) + Math.round(drag * 2) / 2
          data = end_ts: moment(scope.data.start_ts, 'X').hour(_time.hour).minute(_time.minute).startOf('minute').format('X') - 1
        else
          _time = time.parse Number(attrs.y1) + Math.round(drag * 2) / 2
          data = start_ts: moment(scope.data.start_ts, 'X').hour(_time.hour).minute(_time.minute).startOf('minute').format 'X'

        if attrs.hbGridItem is 'reservation'
          updateReservation data
        else if attrs.hbGridItem is 'block'
          updateBlockTimes data

        $(document).off 'mousemove', throttled
        clicked = false
        _scope.itemDrag = false

    throttled = _.throttle _drag.move, 20
    element.on 'mousedown', '.pointer', _drag.start
    element.on 'click', '.pointer', (e) -> e.stopPropagation()

    updateReservation = (data) ->
      api.updateReservation
        params:
          shop_id: shop.data.id
          reservation_id: scope.data.id
        data:
          reservation: _.chain(scope.data).pick(['updated_at']).assign(data).value()
        success: (res) ->
          return if not data = res.data
          _scope.reservations[scope.index] = data
          _scope.redraw()
        error: callback: ->
          scope.attrs.height = 2 * _scope.times.height * (attrs.y2 - attrs.y1) - 2 * scope.padding

    updateBlockTimes = (data) ->
      api.updateBlockTimes
        params:
          shop_id: shop.data.id
          block_id: scope.data.id
        data: data
        success: (res) ->
          if res.refresh
            _scope.redraw true
            return
          return if not data = res.data
          unlockMatrix()
          element.remove()
          _scope.draw.block [data], reset: false
          _scope.blocks[scope.index] = data
        error: callback: -> _.assign scope.attrs,
          y: _scope.weeks.height + 2 * _scope.times.height * attrs.y1 + scope.padding
          height: 2 * _scope.times.height * (attrs.y2 - attrs.y1) - 2 * scope.padding

    unlockMatrix = ->
      y1 = Math.floor(attrs.y1 * 2)
      y2 = Math.ceil(attrs.y2 * 2)
      scope.matrix[attrs.x][y1...y2] = _.map scope.matrix[attrs.x][y1...y2], (y) -> if y is 1 then 0 else y
