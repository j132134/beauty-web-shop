app = require '../app'

regex = translate: /translate\(([-.\d]+), ([-.\d]+)\)/

app.directive 'hbGridXScroll', ($stateParams, $parse, storage) ->
  link: (scope, element) ->
    return if scope.personal

    board = element.siblings '.reservation'
    maxScroll = scope.maxScroll

    clickable = true
    clicked = false
    moved = 0
    startX = 0
    setX = 0
    current = {}
    offset = element.offset()

    scope.$evalAsync ->
      storage.grid.set 'scrollLeft', scope.posX
      element.attr 'transform', "translate(#{scope.posX}, 0)"
    scope.$applyAsync ->
      current = regex.translate.exec board.attr 'transform'
      board.attr 'transform', "translate(#{scope.posX}, #{current?[2] or 0})"

    drag =
      start: (e) ->
        e.stopPropagation()
        return if scope.personal or e.which isnt 1

        startX = e.pageX - offset.left
        return if startX < scope.times.width

        current = regex.translate.exec board.attr 'transform'
        current ?= ['', 0, 0]

        clickable = true
        clicked = true
        moved = 0

      move: (e) ->
        e.stopPropagation()
        return if not clicked

        clickable = false

        moved = e.pageX - offset.left - startX
        setX = Number(current[1]) + moved
        setX = Math.max Math.min(setX, 0), maxScroll

        board.attr 'transform', "translate(#{setX}, #{current[2]})"
        element.attr 'transform', "translate(#{setX}, 0)"

      end: (e) ->
        return if e.which isnt 1 or not clicked or clickable
        storage.grid.set 'scrollLeft', scope.posX = setX
        clicked = false

    element.on 'mousedown', drag.start
    $(document).on 'mousemove', _.throttle drag.move, 20
    $(document).on 'mouseup', drag.end

    element.on 'click', (e) ->
      return if not clickable
      $parse($(e.target).attr 'data-click')(scope)

    scope.$on '$destroy', ->
      element.off 'mousedown'
      $(document).off 'mouseup', drag.end
