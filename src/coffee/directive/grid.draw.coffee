app = require '../app'

moment = require 'moment'
time = require '../utility/time'

app.directive 'hbGridDraw', ($rootScope, $state, $location, $compile, $interval) ->
  controller: ($scope, $element) ->
    columns = $scope.columns
    start = $scope.start
    end = $scope.end
    now = moment()

    $scope.wrapper = wrapper =
      width: $element.width()
      height: $element.height()

    $scope.times = times =
      width: 54
      height: 42
      hour: _.map [0..24], (i) -> _.padStart(i, 2, '0') + '시'

    $scope.weeks = weeks =
      height: 52
      name: ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']
      name_ko: ['일', '월', '화', '수', '목', '금', '토']
      date: _.map [0..6], (i) -> if (date = (day = moment(start).weekday(i)).date()) isnt 1 then date else "#{day.month() + 1}/#{date}"
      weekday: now.weekday()
      isThisWeek: not(now.isBefore(start) or now.isAfter end)
      thisMonth: (if @isThisWeek then now else if start.month() isnt end.month() and start.clone().weekday(3).date() < 7 then end else start).format 'YYYY/MM'

    $scope.xWidth = xWidth = (wrapper.width - times.width) / columns
    $scope.xWidth = xWidth = Math.max 130, xWidth if not $scope.personal
    $scope.xPos = xPos = _.map [0..columns], (i) -> i * xWidth + times.width
    $scope.xCenter = _.map _.initial(xPos), (pos) -> pos + xWidth / 2
    $scope.scrollWidth = times.width + xWidth * columns
    $scope.maxScroll = wrapper.width - $scope.scrollWidth

  compile: ->
    $(window).on 'resize', _.debounce (-> $state.reload()), 150

    return ($scope, $element) ->

      do drawline = ->
        line = $element.find '.now .line'
        marker = $element.find '.now .marker'

        now = moment()
        y = 2 * $scope.times.height * (now.hour() + now.minute() / 60) + $scope.weeks.height

        line.attr 'y1', y
        line.attr 'y2', y
        marker.attr 'cy', y

      updateDrawline = $interval drawline, 120000
      $element.on '$destroy', -> $interval.cancel updateDrawline

      ## draw
      el =
        svgns: 'http://www.w3.org/2000/svg'
        closed: $element.find '.closed'
        blocks: $element.find '.blocks'
        items: $element.find '.items'
      
      $scope.draw =
        closed: (data) ->
          el.closed.empty()
          _.each data, (column, x) -> _.each column, (time) ->
            rect = document.createElementNS el.svgns, 'rect'
            rect.setAttributeNS null, 'x', $scope.xPos[x]
            rect.setAttributeNS null, 'y', $scope.weeks.height + 2 * $scope.times.height * time[0]
            rect.setAttributeNS null, 'width', $scope.xWidth
            rect.setAttributeNS null, 'height', 2 * $scope.times.height * (time[1] - time[0])
            el.closed.append rect

        block: (data, option) ->
          option = _.defaults option, {reset: true}
          el.blocks.empty() if option.reset
          _.each data, (item) ->
            return if not item
            endIndex = time.toIndex moment(item.end_ts, 'X').format 'HHmm'
            startIndex = time.toIndex (start = moment item.start_ts, 'X').format 'HHmm'
            x = if $scope.personal then start.weekday() else _.findIndex $scope.employments, id: item.employment_id
            node = document.createElementNS el.svgns, 'g'
            node.setAttributeNS null, 'data-x', x
            node.setAttributeNS null, 'data-y1', startIndex
            node.setAttributeNS null, 'data-y2', endIndex
            node.setAttributeNS null, 'data-id', item.id
            node.setAttributeNS null, 'hb-grid-item', 'block'
            el.blocks.append $compile(node)($scope)
            blockMatrix x, startIndex, endIndex, 1

        reservation: (data, option) ->
          option = _.defaults option, {reset: true}
          el.items.empty() if option.reset
          _.each data, (item) ->
            startIndex = time.toPoint (start = moment item.start_ts, 'X').format 'HHmm'
            endIndex = Math.min 24, Math.max startIndex + .5, if item.end_ts then time.toPoint moment(item.end_ts, 'X').format 'HHmm' else startIndex + (item.service_duration / 60)
            x = if $scope.personal then start.weekday() else _.findIndex $scope.employments, id: item.employment_id
            node = document.createElementNS el.svgns, 'g'
            node.setAttributeNS null, 'data-x', x
            node.setAttributeNS null, 'data-y1', startIndex
            node.setAttributeNS null, 'data-y2', endIndex
            node.setAttributeNS null, 'data-id', item.id
            node.setAttributeNS null, 'ng-click', "modal.open(#{item.id})"
            node.setAttributeNS null, 'hb-grid-item', 'reservation'
            el.items.append $compile(node)($scope)
            blockMatrix x, startIndex, endIndex, 2

      blockMatrix = (x, y1, y2, value) ->
        y1 = Math.floor(y1 * 2)
        y2 = Math.ceil(y2 * 2)
        $scope.matrix[x][y1...y2] = _.map $scope.matrix[x][y1...y2], (y) -> Math.max y, value
