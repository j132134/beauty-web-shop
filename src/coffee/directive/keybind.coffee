app = require '../app'

app.directive 'hbKeybind', ($document, $rootScope, modal, dialog) ->
  link: (scope) ->
    $document.on 'keydown', (e) ->
      return if $rootScope.drawer.isOpen
      code = e.keyCode
      if dialog.show
        dialog.keydown code
      else if modal.show
        modal.keydown code
      else
        scope.$broadcast 'keydown', code: code
      scope.$apply()
