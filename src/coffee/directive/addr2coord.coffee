app = require '../app'

app.directive 'hbAddr2coord', ($http, notify) ->
  scope:
    focus: '&'
    blur: '&'
  link: (scope, element) ->
    element.on 'focus', ->
      scope.focus()
      scope.$apply()

    element.on 'blur', ->
      return if not query = element.val()
      $http.jsonp 'https://apis.daum.net/local/geo/addr2coord?callback=JSON_CALLBACK',
        params:
          apikey: '7858e8ffaf41c136627ef6384bf8bb1b' or '01b48c16694e88093622fd45302f0cac'
          output: 'json'
          page_size: 30
          q: query
      .then (res) ->
        if (count = Number res.data?.channel?.result) < 1
          notify.warn '검색결과가 없습니다. 다시 검색해 주세요.'
        else if count > 1
          notify.warn '주소가 정확하지 않습니다. 다시 검색해 주세요.'
        else
          data = res.data.channel.item[0]
          if data.isNewAddress is 'Y'
            addr = data.title
            jibun = data.newAddress
          else
            addr = "#{data.localName_1} #{data.localName_2} #{data.newAddress}"
            jibun = "#{data.localName_3} "
            jibun += "#{data.mainAddress}"
            jibun += "-#{data.subAddress}" if Number(data.subAddress) isnt 0
          scope.blur obj: {addr: addr, jibun: jibun, lat: data.lat, lng: data.lng}