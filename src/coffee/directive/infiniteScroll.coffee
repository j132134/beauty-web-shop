app = require '../app'

app.directive 'hbInfiniteScroll', ($interval) ->
  scope:
    load: '&hbInfiniteScroll'
    stop: '=hbInfiniteStop'
    end: '=hbInfiniteEnd'

  link: (scope, element) ->
    scrollable = ->
      element[0].scrollHeight - element.scrollTop() - element.outerHeight() > window.screen.availHeight * .3
      
    load = $interval ->
      return if scrollable() or scope.stop
      scope.load()
    , 300

    watcher = scope.$watch 'end', (end) ->
      return if not end
      $interval.cancel load
      watcher()

    scope.$on '$destroy', ->
      $interval.cancel load
      watcher()
