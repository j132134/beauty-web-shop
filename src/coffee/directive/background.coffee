app = require '../app'

app.directive 'hbBackgroundImage', ->
  scope: true
  link: (scope, element, attrs) ->
    return if not /\.(jpe?g|png|gif)$/i.test url = attrs.hbBackgroundImage

    $.ajax
      type: 'HEAD'
      url: url
      success: -> insert()
      error: ->
        image = new Image()
        image.onload = -> insert()
        image.onerror = -> setTimeout (-> image.src = url), 500
        image.src = url

    insert = -> element.css 'background-image': "url(#{url})"
