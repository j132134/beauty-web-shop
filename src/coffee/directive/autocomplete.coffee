app = require '../app'

parser = require '../utility/parser'

app.directive 'hbAutocomplete', ($timeout, $parse) ->
  template: ->
    '<div>' +
      '<ng-transclude></ng-transclude>' +
      '<ul class="dropdown"ng-show="searched.length">' +
        '<li ng-repeat="item in searched" ng-bind="item[label]" ng-click="select(item)" ng-class="{active: $index === index}"></li>' +
      '</ul>' +
    '</div>'
  transclude: true
  link: (scope, element, attrs) ->
    scope.label = attrs.label
    scope.index = -1

    input = element.find 'input'
    dropdown = element.find '.dropdown'
    options = []
    labels = []
    required = Boolean attrs.hbAutocomplete
    own =
      data: $parse(input.attr('ng-model'))(scope)
      getData: -> if @data then _.zipObject([attrs.label], [@data]) or undefined
    previous = own.getData()

    _callback = $parse(attrs.callback)(scope)
    callback = (data) ->
      input.val own.data = data?[attrs.label]
      _callback data

    element.on 'keydown', (e) ->
      switch e.keyCode
        when 38
          scope.index-- if scope.index > 0
          scroll 'up'
        when 40
          scope.index++ if scope.index < labels.length - 1
          scroll 'down'
        when 13
          scope.select scope.searched[scope.index] if ~scope.index
          input.blur()
      scope.$apply()

    scrolled = false
    scroll = (direction) ->
      index = scope.index
      length = labels.length
      scrollHeight = dropdown.prop 'scrollHeight'
      visible = Math.floor(dropdown.height() / dropdown.prop('scrollHeight') * length)
      if direction is 'down' and (index + 1) > visible
        min = scrollHeight * (index + 1 - visible) / length
        dropdown.scrollTop min if dropdown.scrollTop() < min
        scrolled = true
      else if direction is 'up' and index < (length - visible)
        max = scrollHeight * (index - 1) / length
        dropdown.scrollTop max if dropdown.scrollTop() > max
        scrolled = true

    element.on 'mousemove', (e) ->
      if scrolled then scrolled = false; return
      return if (_index = element.find('li').index e.target) is -1
      scope.index = _index
      scope.$apply()

    watcher = scope.$watch attrs.options, (raw) ->
      return if not options = raw
      labels = _.map raw, (item) -> parser.korean item[attrs.label]

    input.on 'focus', ->
      scope.index = -1
      input.val ''
      scope.searched = options
      scope.$apply()

    input.on 'keydown', (e) ->
      if e.keyCode is 27
        e.stopPropagation()
        input.blur()

    input.on 'keyup', (e) ->
      scope.index = -1 if not (37 <= e.keyCode <= 40)
      _value = input.val()
      value = parser.korean _value
      callback previous = _.zipObject [attrs.label], [_value]
      if value.length
        indexes = _.without _.map(labels, (item, index) -> return index if _.includes item, value), undefined
        scope.searched = _.filter options, (o, i) -> return _.includes indexes, i
      else
        scope.searched = options
      scope.$apply()

    input.on 'blur', ->
      scope.$applyAsync ->
        _val = input.val()
        if required
          if find = _.find options, [attrs.label, _val]
            callback previous = find
          else if _val.length
            callback null
          else
            callback own.getData()
        else
          callback previous if previous and not input.val().length
        $timeout ->
          scope.searched = []
          dropdown.scrollTop 0
        , 100

    scope.select = (item) ->
      scope.searched = []
      dropdown.scrollTop 0
      callback previous = item
      $timeout (-> input.blur()), 100

    element.on '$destroy', ->
      scope.searched = []
      watcher()
