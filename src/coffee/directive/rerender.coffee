app = require '../app'

app.directive 'hbRerender', ($compile) ->
  compile: ->
    pre: (scope, element, attrs) ->
      template = element.html()

      scope.$watch attrs.hbRerender, ->
        element.html $compile(template) scope
      , true