app = require '../app'

# load service & directive
requireDir = (context) -> context.keys().forEach context
requireDir require.context '../service/', false, /\.coffee$/
requireDir require.context '../directive/', false, /\.coffee$/
requireDir require.context '../filter/', false, /\.coffee$/

state = require '../utility/state'
parser = require '../utility/parser'

app.run ($rootScope, localStorageService, session) ->
  latest = '0.1.1'
  saved = localStorageService.get 'version'
  if saved isnt latest
    session.destroy()
    $rootScope.updated = true if saved
    localStorageService.clearAll()
    localStorageService.set 'version', latest
  $rootScope.headers = null

app.controller 'appController', ($scope, $rootScope, $state, $timeout, $window, $http, server, session, notify, shop, apiCache, modal) ->
  $rootScope.locale = 'ko'

  $scope.modal = modal
  $scope.server = server
  $scope.$applyAsync -> $scope.drawer.init()

  lastest = undefined
  do getVersion = ->
    $http.get '/version.json?v=' + (new Date).getTime()
    .then (res) ->
      _lastest = res.data?.lastest
      if lastest and _lastest > lastest
        alert '앱이 업데이트되어 최신버전으로 새로고침 됩니다.'
        $window.location.reload()
      lastest = _lastest
      $timeout getVersion, res.data?.period

  $scope.$on '$stateChangeSuccess', (event, toState) ->
    shopName = shop.current?.name or 'HeyBeauty'
    $scope.state = toState
    $scope.subtitle = subtitle = state.getTitle name = toState.name
    $scope.title = if subtitle then "#{shopName} - #{subtitle}" else shopName
    $scope.drawer.update name
    $rootScope.hidemenu = name.split('.')[0] isnt 'shop'

  $scope.$watch (-> shop.data), (data) ->
    return if not _.isObject data
    $scope.employment = data?.employments.length
    $rootScope.unmanaged = $scope.unmanaged = not data?.is_managed
    $rootScope.disuse = $scope.disuse = $scope.unmanaged or not $scope.employment
    apiCache.reset()

  $scope.shop =
    init: ->
      @keyword = ''
      @own = _.chain($rootScope.auth?.shops).filter((shop) -> shop.shop_status > 4).sortBy('shop_name').value()
      @list = @own

    filter: (keyword) ->
      if keyword?.length
        keyword = parser.korean _.escapeRegExp keyword
        regex = new RegExp "^#{keyword}| #{keyword}"
        @list = _.filter @own, (shop) ->
          return regex.test parser.korean shop.shop_name
      else
        @list = @own

    switching: (target, callback) ->
      shop.setDefault target
      apiCache.reset()
      callback?()
      $scope.$applyAsync -> $state.reload()

  $scope.signout = ->
    session.destroy()
