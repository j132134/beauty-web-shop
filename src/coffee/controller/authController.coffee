app = require '../app'

validator = require '../utility/validator'

app.controller 'signInController', ($scope, $state, session, notify, api) ->
  $scope.user =
    name: ''
    phone: ''
    code: ''

  $scope.requested = false

  $scope.requestCode = ->
    phone = $scope.user.phone.replace /\D/g, ''

    if not validator.phone phone
      notify.warn '정확한 핸드폰 번호를 입력해 주세요.'
    else if $scope.requested
      notify.warn '이미 인증번호가 전송 되었습니다.'
    else
      api.smsAuth
        params:
          phone_num: phone
        success: ->
          $scope.requested = true
          notify.info '인증번호가 전송되었습니다.'
        error:
          message: '인증번호 요청에 실패했습니다. 잠시 후 다시 시도해 주세요.'

  $scope.signin = ->
    phone = $scope.user.phone.replace /\D/g, ''
    code = $scope.user.code

    if not validator.phone phone
      notify.warn '정확한 핸드폰 번호를 입력해 주세요.'
    else if validator.isNull code
      notify.warn '인증번호를 입력해 주세요.'
    else
      api.signin
        data:
          phone_num: phone
          sms_auth_code: code
        success: (res) ->
          session.save res.data
          $scope.$applyAsync -> $state.transitionTo 'shop.dashboard', null, {location: 'replace', reload: true}
        error:
          message: '인증번호가 올바르지 않습니다. 입력하신 정보를 다시 확인해주세요.'


app.controller 'signUpController', ($scope, $rootScope, $state, session, notify, shop, api, meta) ->
  $scope.categories = meta.categories
  $scope.regions = meta.regions

  $scope.form = form =
    shopName: ''
    category: null
    region: null
    ownerName: ''
    ownerPhone: ''

  $scope.signup = ->
    data =
      name: form.shopName
      category_id_list: [form.category]
      category_ids: [form.category]
      region_id: form.region
      owner_name: form.ownerName
      owner_phone: form.ownerPhone.replace /\D/g, ''

    if validator.isNull data
      notify.warn '입력하지 않은 항목이 있습니다.'
    else
      api.signup
        data: data
        success: (res) ->
          if $rootScope.auth?.is_peon and (data = res.data)?.id
            shop.setDefault {shop_id: data.id, shop_name: data.name}
            $scope.$applyAsync -> $state.go 'shop.setting', null, {reload: true}
          else
            notify.success '등록되었습니다. 입력하신 핸드폰 번호로 로그인 해주세요.'
            session.destroy notify: false

