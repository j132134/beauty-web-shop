app = require '../app'

app.controller 'postController', ($scope, own, api, notify) ->

  $scope.use = true
  $scope.done = true
  $scope.keyword = own.keywords
  $scope.result = []
  page = 0

  do getPosts = ->
    api.getPosts
      params:
        shop_id: own.id
        use: $scope.use
        shop_managed: false
      success: (res) ->
        $scope.data = _.orderBy res.data, ['published_ts', 'id'], ['desc', 'asc']

  $scope.setState = (value) ->
    $scope.use = value
    getPosts()

  $scope.searchPosts = (reset) ->
    if reset
      page = 0
      $scope.result = []
    else
      return if $scope.done

    return if not $scope.keyword = $scope.keyword?.trim()
    $scope.done = true
    api.searchPosts
      params:
        shop_id: own.id
        keyword: keyword = if reset then $scope.keyword else own.keywords
        page: page++
      success: (res) ->
        own.keywords = keyword
        $scope.done = res.done
        $scope.result = _.concat $scope.result, res.data

  $scope.createPost = (id, use, index = 0) ->
    api.createPost
      params:
        shop_id: own.id
        id: id
        use: use
        image_index: index
      success: ->
        notify.success if use then '추가되었습니다.' else '사용안함 처리 되었습니다.'
        _.remove $scope.result, id: id
        getPosts()

  $scope.togglePost = (id) ->
    api.updatePost
      params:
        shop_id: own.id
        id: id
        use: not $scope.use
      success: ->
        notify.success "#{if $scope.use then '사용안함' else '사용'} 처리 되었습니다."
        _.remove $scope.data, id: id

  $scope.updatePost = (id, index) ->
    api.updatePost
      params:
        shop_id: own.id
        id: id
        image_index: index
      success: ->
        notify.success '대표이미지가 변경 되었습니다.'
        postIndex = _.findIndex $scope.data, id: id
        $scope.data[postIndex].main_image_url = $scope.data[postIndex].images[index].hb_thumb

  $scope.addBlacklist = (id, username) ->
    api.addPostBlacklist
      params:
        shop_id: own.id
        id: id
      success: ->
        notify.success '블랙리스트에 추가 되었습니다.'
        _.remove $scope.result, username: username
