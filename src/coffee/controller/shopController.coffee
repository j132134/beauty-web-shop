app = require '../app'

app.controller 'shopController', ($scope, $rootScope, $state, $interval, $timeout, own, notify, modal, api) ->

  $scope.$applyAsync -> $scope.$parent.shop.init()

  tritone = new Audio 'http://assets.heybeauty.me/kr/web/tritone.wav'

  $rootScope.inbox = inbox = []

  api.getReservations
    server: 'api'
    params:
      shop_id: own.id
      status_eq: ['pending']
    success: (res) ->
      return if not res.data?.length
      data = _.orderBy res.data, ['updated_at'], ['asc']
      $rootScope.inbox = inbox = _.uniq _.concat inbox, _.map data, 'id'

  last_updated_ts = $rootScope.init_ts
  getUpdate = ->
    api.getReservations
      server: 'api'
      params:
        shop_id: own.id
        last_updated_ts: last_updated_ts
      success: (res) ->
        last_updated_ts = res.meta.updated_ts
        return if _.isEmpty data = res.data
        $scope.$broadcast 'reservation:update'
        pending = _.filter data, status: 'pending'
        if pending.length
          tritone.currentTime = 0
          tritone.play()
          $rootScope.inbox = inbox = _.uniq _.concat inbox, _.map pending, 'id'
        if modal.show and ~(index = _.findIndex data, id: modal.item.id) and data[index].updated_at isnt modal.item.updated_at
          notify.info '예약정보가 수정되어 다시 로드되었습니다.' if modal.isEdit
          modal.open data[index]
  
  polling = $interval ->
    getUpdate()
  , 7 * 1000

  $scope.$on '$destroy', ->
    $interval.cancel polling
