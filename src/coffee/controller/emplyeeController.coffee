app = require '../app'

moment = require 'moment'
schedule = require '../utility/schedule'
time = require '../utility/time'
parser = require '../utility/parser'
validator = require '../utility/validator'

app.controller 'employeeController', ($scope, $rootScope, $state, $stateParams, $timeout, own, employments, notify, api, apiCache) ->

  $scope.employments = employments
  if not $scope.id = employment_id = $stateParams.id then $state.transitionTo $state.current, {id: employments[0].id}, {location: 'replace'}

  open = _open = 0
  close = _close = 24
  if Boolean (close - open) % 2 then (if close is 24 then _open -= 1 else _close += 1)

  $scope.weeks = ['일', '월', '화', '수', '목', '금', '토']
  $scope.hours = _.map _.range(_open, _close + 1), (h) -> _.padStart h, 2, '0'

  $scope.table = _table = {width: 380, height: 160}
  _.assign _table, weeks: {height: 32}
  _.assign _table, times: {width: 46, height: (_table.height - _table.weeks.height - 10) / (_close - _open)}
  _.assign _table.weeks, {width: (_table.width - _table.times.width) / 7}

  $scope.employee_name = _.find(own.employments, id: employment_id)?.employee_name
  if employment_id then api.getEmployment
    params:
      shop_id: own.id
      employment_id: employment_id
    success: (res) ->
      $scope.form = schedule.modelize res.data?.employment_schedules or []
    error:
      silent: true
      callback: -> $state.transitionTo $state.current, {id: employments[0].id}, {location: 'replace'}

  $scope.$watch 'form', (newData, oldData) ->
    fail = []
    data = _.reject schedule.normalize(newData), (item) -> item.startIndex >= item.endIndex

    _.chain(data).groupBy((item) -> item.day).each (items) ->
      return if items.length is 1
      _times = _.chain(items).sortBy('startIndex').flatMap((item) -> return [item.startIndex, item.endIndex]).value()
      fail.push items[0].day if not _.isEqual _times, _.chain(_times).clone().sortBy().value()
    .value()

    if fail.length
      notify.warn "#{_.chain(fail).map((d) -> $scope.weeks[d]).join(',').value()}요일 근무시간이 중복됩니다."
      $timeout (-> $scope.form = oldData), 350
    else
      $scope.data = _.each data, (item) ->
        if item.startIndex < open
          item.startIndex = open
          item.start = time.toString open, 'HHmm'
        if item.endIndex > close
          item.endIndex = close
          item.end = time.toString close, 'HHmm'
  , true

  $scope.isEdit = false

  $scope.timePicker =
    hours: _.range(0, 25, 3)
    index: 0
    string: ''
    edit: (@where, @target) ->
      _time = $scope.form[@where][@target].replace /\D/g, ''
      @index = time.toIndex(_time) * 2
    pick: (hour) ->
      @index = hour * 2
    confirm: ->
      $scope.form[@where][@target] = parser.time time.toString @index / 2, 'HHmm'
      if @target is 'start' and not Number $scope.form[@where].end.replace /\D/g, ''
        $timeout (=> $scope.timePicker.edit @where, 'end'), 200
      @target = null

  $scope.$watch 'timePicker.index', (index) ->
    $scope.timePicker.string = parser.time time.toString index / 2, 'HHmm'

  $scope.add = -> $scope.form.push {
    days: _.chain().range(7).fill(false).value()
    start: '0000'
    end: '0000'
  }

  $scope.remove = (index) ->
    _.pullAt $scope.form, index

  $scope.save = ->
    api.updateEmploymentSchedule
      params:
        shop_id: own.id
        employment_id: employment_id
      data:
        employment_schedules_attributes: _.map $scope.data, (item) -> _.omit item, ['startIndex', 'endIndex']
      success: (res) ->
        return if res.success is false
        notify.success '저장 되었습니다.'
        _.assign employments[_.findIndex employments, id: employment_id],
          schedules: angular.copy $scope.form = schedule.modelize res.data.employment_schedules
          employment_schedules: res.data.employment_schedules

  $scope.edit =
    isEdit: false
    item: {}

    new: ->
      @isEdit = true
      @isNew = true
      @item =
        role: 'employee'
        status: 'hired'
        accept_reservation: true
        recieve_push: true

    enter: (id) ->
      @isEdit = true
      @item = _.clone _.find $scope.employments, id: id

    save: ->
      if message = validator.employment @item
        notify.error message
        return

      (if @isNew then api.addEmployment else api.updateEmployment)(
        params:
          shop_id: own.id
          employment_id: @item.id
        data: _.assign @item, phone_num: @item.phone_num.replace /\D/g, ''
        success: =>
          notify.success '저장되었습니다.'
          if @isNew or @item.status is 'quit'
            apiCache.reset()
            $scope.$evalAsync -> $state.reload()
          else
            _.assign employments[_.findIndex $scope.employments, id: @item.id], @item
            @exit()
      )

    exit: ->
      @isEdit = false
      @isNew = false
      @item = {}

  $scope.$on 'keydown', (e, args) ->
    return if not $scope.edit.isEdit
    switch args.code
      when 27 # esc
        $scope.edit.exit()
      when 13 # enter
        $scope.edit.save()
