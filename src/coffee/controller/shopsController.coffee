app = require '../app'

app.controller 'shopsController', ($scope, $rootScope, $state, $stateParams, shop, notify) ->
  if not $rootScope.auth?.shops
    notify.error '샵 리스트를 찾을 수 없습니다.'
    return

  $scope.manage = manage = $stateParams.manage is 'manage'

  korean = [0x3131, 0x3132, 0x3134, 0x3137, 0x3138, 0x3139, 0x3141, 0x3142, 0x3143, 0x3145, 0x3146, 0x3147, 0x3148, 0x3149, 0x314a, 0x314b, 0x314c, 0x314d, 0x314e]
  $scope.consonants = consonants = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ', '#'
  ]

  shops = _.chain $rootScope.auth.shops
    .clone()
    .filter (shop) -> if manage then shop.shop_status >= 4 else shop.shop_status < 4
    .sortBy 'shop_name'
    .map (shop) ->
      charCode = shop.shop_name?.split('')[0].charCodeAt(0)
      if 44032 <= charCode <= 55203 then charCode = korean[parseInt((charCode - 44032) / 588)]
      if 97 <= charCode <= 122 then charCode -= 32
      _.assign shop, charCode: charCode
    .value()

  $scope.list = _.map consonants, (consonant, i) ->
    if i isnt consonants.length - 1
      return _.remove shops, charCode: consonant.charCodeAt(0)
    else
      return shops

  $scope.switch = (id) ->
    shop.setDefault _.find $rootScope.auth.shops, shop_id: id
    notify.success '변경되었습니다.'
    $state.reload()
