app = require '../app'

validator = require '../utility/validator'
schedule = require '../utility/schedule'

app.controller 'settingController', ($scope, $rootScope, $state, $http, $timeout, own, notify, dialog, api, meta, shop) ->

  $scope.categories = meta.categories
  $scope.regions = meta.regions
  $scope.stations = _.map meta.stations, (station) -> name: station

  $scope.form = form = _.clone own

  complete = true

  $scope.notify = (message) ->
    notify.warn message

  $scope.editStation = (station) ->
    $scope.form.subway_station = station?.name or null

  $scope.resetAddress = ->
    complete = false

  $scope.insertAddress = (obj) ->
    complete = true
    _.assign $scope.form,
      address: obj.addr
      address_jibun: obj.jibun
      latitude: obj.lat
      longitude: obj.lng

  $scope.regex = regex =
    naver_blog: '^[a-z0-9_-]+$'
    modoo: '^[a-z0-9]+$'
    instagram: '^[a-zA-Z0-9_.]+$'
    facebook: '^[a-zA-Z0-9.]+$'
    homepage: '^(?:https?:\/\/)?(.*)$'

  $scope.submit = ->
    if validator.isNull _.pick form, ['region_id', 'business_hours', 'phone_num', 'address']
      notify.warn '입력하지 않은 항목이 있습니다.'

    else if not form.category_id_list.length
      notify.warn '서비스 업종을 선택해주세요.'

    else if not complete
      notify.warn '정확한 주소를 입력해주세요.'

    else if not _.includes meta.stations, form.subway_station
      notify.warn '올바른 지하철역 이름이 입력해주세요'
      $scope.form.subway_station = null

    else
      form = validator.shopInfo form, regex
      shop.updateInfo form, (data) ->
        notify.success '저장되었습니다.'
        _.assign own, data
