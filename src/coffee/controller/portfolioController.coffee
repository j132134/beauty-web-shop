app = require '../app'

app.controller 'portfolioController', ($scope, $rootScope, own, api, notify) ->

  do getPortfolios = (callback = null) ->
    api.getPortfolios
      params:
        shop_id: own.id
      success: (res) ->
        return if not data = res.data
        $scope.shop_images = _.reject data.shop_images, image_url: null
        $scope.portfolios = _.reject data.portfolios, image_url: null
        callback?()

  do getPosts = ->
    api.getPosts
      params:
        shop_id: own.id
        shop_managed: true
      success: (res) ->
        $scope.posts = _.orderBy res.data, ['published_ts', 'id'], ['desc', 'asc']

  $scope.updateOrder = (ids) ->
    api.updatePortfolioOrder
      params:
        shop_id: own.id
      data:
        ordered_id_list: ids
      error: callback: -> getPortfolios()
      success: ->
        notify.success '저장되었습니다.'

  $scope.uploadImage = (data, shop_image = true) ->
    api.createPortfolio
      params:
        shop_id: own.id
      data:
        shop_image: shop_image
        image_base_64: data
      success: (res) ->
        getPortfolios ->
          if shop_image
            edit.shop_image res.data?.id
          else
            edit.portfolio res.data?.id

  $scope.updatePost = (id, use) ->
    api.updatePost
      params:
        shop_id: own.id
        id: id
      data:
        use: use
      success: ->
        index = _.findIndex $scope.posts, id: id
        $scope.posts[index].use = use

  $scope.edit = edit =
    services: own.services
    isEdit: false
    isNew: false
    item: {}
    index: -1

    shop_image: (id) ->
      @isEdit = true
      @key = 'shop_images'
      @index = _.findIndex $scope.shop_images, id: id
      @item = _.clone $scope.shop_images[@index]
      @item.post = _.find $scope.posts, id: postId if postId = @item?.post_id
      @item.hashtag_list = @item.tags?.join ', ' or ''
      if not @item then @exit()

    portfolio: (id) ->
      @isEdit = true
      @key = 'portfolios'
      @index = _.findIndex $scope.portfolios, id: id
      @item = _.clone $scope.portfolios[@index]
      @item.post = _.find $scope.posts, id: postId if postId = @item?.post_id
      @item.hashtag_list = @item.tags?.join ', ' or ''
      if not @item then @exit()

    new: (id, index) ->
      post = _.find $scope.posts, id: id
      @isEdit = true
      @isNew = true
      @item =
        title: ''
        image_url: post.images[index].hb_thumb
        service_id: null
        post_id: id
        post: post
        shop_image: false
        image_index: index

    create: ->
      api.createPortfolio
        params: _.chain(@item).pick(['title', 'shop_image', 'hashtag_list', 'service_id', 'post_id', 'image_index']).assign(shop_id: own.id).value()
        success: (res) =>
          getPosts()
          getPortfolios =>
            @isNew = false
            if res.data?.shop_image
              @shop_image res.data?.id
            else
              @portfolio res.data?.id

    save: ->
      api.updatePortfolio
        params:
          shop_id: own.id
          id: @item.id
        data: _.pick @item, ['title', 'shop_image', 'hashtag_list', 'service_id']
        success: (res) =>
          notify.success '저장되었습니다.'
          index = _.findIndex $scope[@key], id: @item.id
          if @item.shop_image is $scope[@key][index].shop_image
            $scope[@key][index] = res.data
          else
            @key = if @key is 'portfolios' then 'shop_images' else 'portfolios'
            getPortfolios()

    delete: ->
      api.deletePortfolio
        params:
          shop_id: own.id
          id: @item.id
        success: =>
          getPosts()
          _.remove $scope[@key], id: @item.id
          notify.success '삭제되었습니다.'
          @exit()

    prev: ->
      if @index is 0
        notify.warn '첫 이미지 입니다.'
      else
        @[@key.replace(/s$/, '')] $scope[@key][--@index].id

    next: ->
      if @index is $scope[@key].length - 1
        notify.warn '마지막 이미지 입니다.'
      else
        @[@key.replace(/s$/, '')] $scope[@key][++@index].id

    exit: ->
      @isEdit = false
      @isNew = false
      @item = {}
      @index = -1

  $scope.request = ->
    api.requestPortfolio
      params: shop_id: own.id
      success: -> notify.success '승인 요청이 완료 되었습니다.'

  $scope.publish = ->
    return if not $rootScope.auth.is_peon
    api.publishPortfolio
      params: shop_id: own.id
      success: -> notify.success '발행 되었습니다.'

  $scope.scrap = ->
    return if not $rootScope.auth.is_peon
    api.scrapManagedPost
      params: shop_id: own.id
      success: ->
        getPosts()
        notify.success '완료 되었습니다.'

  $scope.$on 'keydown', (e, args) ->
    return if not edit.isEdit
    switch args.code
      when 27 # esc
        edit.exit()
      when 13 # enter
        edit.save()
      when 37 # left
        edit.prev()
      when 39 # right
        edit.next()