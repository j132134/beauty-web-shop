app = require '../app'

moment = require 'moment'

app.controller 'dashboardController', ($scope, $rootScope, $interval, modal, own, api) ->

  do getClock = ->
    now = moment()
    $scope.clock =
      date: now.format 'M월 D일 dddd'
      meridiem: now.format 'A'
      hours: now.format 'hh'
      minutes: now.format 'mm'
  $interval getClock, 500

  day = moment().add 1, 'months'
  $scope.reservations = []
  $scope.getReservations = ->
    $scope.loading = true
    api.getReservations
      params:
        shop_id: own.id
        end_ts: day.endOf('month').format 'X'
        start_ts: day.subtract(1, 'months').startOf('month').format 'X'
        state: 'dashboard'
      error: silent: true
      success: (res) ->
        day.subtract 1, 'months'
        $scope.loading = false
        $scope.end = Boolean day.isBefore moment(own.first_reservation_ts, 'X'), 'day'

        return if not data = res.data

        data = _.chain data
        .each (item) ->
          item.end_ts = if item.end_ts then item.end_ts + 1 else Number moment(item.start_ts, 'X').add(item.service_duration, 'm').format 'X'
          _.pull $rootScope.inbox, item.id
        .groupBy (item) -> moment(item.start_ts, 'X').format 'YYYY.MM.DD'
        .transform ((result, value, key) -> result.push {date: key, items: value}), []
        .value()

        $scope.reservations = $scope.reservations.concat data

  $scope.$on 'reservation:update', ->
    day = moment().add 1, 'months'
    $scope.reservations = []
    $scope.loading = false
    $scope.end = false

  $scope.open = (item) ->
    modal.open item