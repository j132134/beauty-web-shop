app = require '../app'

app.controller 'switchController', ($scope, $rootScope, $state, $stateParams, notify) ->

  redirect = $stateParams.redirect or 'dashboard'

  if shop = _.find $rootScope.auth.shops, ['shop_id', Number $stateParams.id]
    $scope.$parent.shop.switching shop, ->
      $state.transitionTo "shop.#{redirect}", null, {location: 'replace'}
  else
    notify.error '없는 상점이거나 접근 권한이 없습니다.'
    $state.go 'shop.reservation'
