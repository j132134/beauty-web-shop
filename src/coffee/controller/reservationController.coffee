app = require '../app'

moment = require 'moment'
time = require '../utility/time'
schedule = require '../utility/schedule'

app.controller 'reservationController', ($scope, $state, $stateParams, $interval, $timeout, own, shop, modal, storage, notify, api) ->

  $scope.personal = personal = not daily = _.isUndefined employment_id = $stateParams.employee
  $scope.employments = employments = if own.employments.length > 1 then [{id: null, employee_name: '미지정'}].concat(own.employments) else own.employments
  $scope.employment = employment = _.find(own.employments, id: Number employment_id)
  $scope.columns = columns = if personal then 7 else employments.length
  employment_ids = _.chain(employments).map('id').concat([null]).uniq().value()

  date = $stateParams.date?.replace /(\d{4})(\d{2})(\d{2})/, '$1-$2-$3'
  if personal
    $scope.start = start = moment(date).startOf 'week'
    $scope.end = end = moment(date).endOf 'week'
    closed = schedule.convert $scope.employment?.data
  else
    $scope.start = start = moment(date).startOf 'day'
    $scope.end = end = moment(date).endOf 'day'
    closed = schedule.convert employments, start.weekday()

  if personal and not employment
    return $state.go $state.current, null, {inherit: false, location: 'replace'}
  if daily and employments.length is 1
    return $state.go $state.current, employee: employments[0].id, {inherit: true, location: 'replace'}

  $scope.$evalAsync ->
    $scope.draw.closed closed
    resetMatrix()

  resetMatrix = -> $scope.matrix = _.range(columns).map -> _.times 48, _.constant 0
  evaluate = ->
    reservations = _.chain($scope.reservations).clone().sortBy('start_ts').groupBy(daily and 'employment_id').value()
    _.each reservations, (employee_reservations) ->
      evaluated = []
      buffers = _.clone employee_reservations
      last_ts = 0
      current = 0

      while buffers.length
        index = _.findIndex buffers, (o) -> o.start_ts >= last_ts
        if index > -1
          evaluated.push next = _.pullAt(buffers, index)[0]
          next.end_ts ?= Number moment(next.start_ts, 'X').add(next.service_duration, 'm').format 'X'
          if next.end_ts < next.start_ts + 1800 then next.end_ts = next.start_ts + 1800
          next.extended = degree: current, total: current + 1, lefts: []
          next.extended.lefts = _.filter evaluated, (o) -> o.end_ts > next.start_ts and o.start_ts < next.end_ts and o.id isnt next.id
          $scope.reservations[_.findIndex $scope.reservations, id: next.id] = next

          (retroact = (array) ->
            _.each array, (item) ->
              item.extended.total = current + 1
              retroact item.extended.lefts
          ) next.extended.lefts

          last_ts = next.end_ts
        else
          last_ts = 0
          current++

      _.each evaluated, (o) ->
        o.extended.total = _.maxBy(o.extended.lefts, (i) -> i.extended.total)?.extended.total or o.extended.total

  $scope.redraw = (reload = false) ->
    if reload
      getReservations true
    else
      resetMatrix()
      $scope.draw.block $scope.blocks
      $scope.draw.reservation $scope.reservations
      evaluate()

  do getReservations = (silent = false) ->
    api.getReservations
      indicator: not silent
      params:
        shop_id: own.id
        start_ts: start.format 'X'
        end_ts: end.format 'X'
        employment_id: personal and employments.length > 1 and employment_id or null
      success: (res) ->
        return if not data = res?.data
        resetMatrix()
        data = _.filter data, (item) -> ~employment_ids.indexOf item.employment_id
        $scope.draw.block $scope.blocks = res.meta?.block_times
        $scope.draw.reservation $scope.reservations = _.reject data, status: 'canceled'
        evaluate()
      error:
        message: '데이터를 가져올 수 없습니다. 잠시 후 다시 시도해주세요.'
        silent: silent

  polling = $interval (-> getReservations true), 10 * 60 * 1000
  $scope.$on '$destroy', -> $interval.cancel polling
  $scope.$on 'reservation:update', -> getReservations true

  term = if personal then 7 else 1
  $scope.setDate =
    move: (date = undefined) ->
      $state.go $state.current, date: date, {inherit: true, location: 'replace', reload: $state.current}
    prev: ->
      @move moment(date).subtract(term, 'd').format 'YYYYMMDD'
    next: ->
      @move moment(date).add(term, 'd').format 'YYYYMMDD'
    today: ->
      storage.grid.delete 'scrollTop'
      @move()

  $scope.setEmployee = (id = undefined) ->
    return if employments.length is 1 or id is employment_id
    $state.go $state.current, employee: id, {inherit: true, reload: $state.current}

  $scope.datePicker =
    open: -> @opened = true
    opened: false
    string: moment(date).format 'YYYYMMDD'
    today: moment(date).format 'MMM Do dddd'

  $scope.$watch 'datePicker.string', (string) ->
    return if moment(string, 'YYYYMMDD').isSame moment(date), 'day'
    $state.go $state.current, date: string, {inherit: false, location: 'replace', reload: $state.current}

  $scope.functions =
    show: false
    type: 'normal'
    position: {}
    selected: {}
    callback: null
    reset: ->
      @show = false
      @type = 'normal'
      @position = {}
      @selected = {}
      @callback = null
    create: -> modal.create Number(@selected.start?.ts) or null
    block: ->
      api.addBlockTimes
        params:
          shop_id: own.id
        data:
          employment_id: @selected.employment_id
          start_ts: @selected.start_ts
          end_ts: @selected.end_ts - 1
        success: (res) ->
          return if not data = res.data
          $scope.draw.block [data], reset: false
          $scope.blocks.push data
          $scope.release()
    unlock: ->
      api.removeBlockTimes
        params:
          shop_id: own.id
          block_id: @selected.id
        success: =>
          @callback?()
          @reset()

  $scope.modal = open: (id) -> modal.open _.find $scope.reservations, id: id

  $scope.$on '$stateChangeStart', (e, toState) ->
    storage.grid.remove() if not $state.is toState

  $timeout (-> $state.reload()), moment().endOf('day').format('x') - moment().format('x') + _.random(5, 600) * 1000
