app = require '../app'

moment = require 'moment'
validator = require '../utility/validator'

app.controller 'serviceController', ($scope, own, notify, api) ->

  modelize = (item) ->
    discount_price = if item.discount_type is 'rate' then item.discount_rate else item.discount_amount
    event_start = if item.event_start_at then moment(item.event_start_at, HB.time_format) else null
    event_end = if item.event_end_at then moment(item.event_end_at, HB.time_format) else null
    is_event = Boolean(discount_price) and (event_start and event_end) and moment().isSameOrBefore(event_end, 'day')
    _.assign item,
      keys: _.keys item if not item.keys
      category_id: item.service_category_id or null
      category_name: _.find(own.service_categories, id: item.service_category_id)?.name or '미분류'
      service_name: item.service_name or item.name
      discount_price: if is_event then discount_price else 0
      discount_rate: if is_event then item.discount_rate else 0
      discount_amount: if is_event then item.discount_amount else 0
      is_event: is_event
      event_start: if is_event then event_start?.format 'YYYY/MM/DD' else null
      event_end: if is_event then event_end?.format 'YYYY/MM/DD' else null

  normalize = (item) ->
    discount_price = Number item.discount_price?.toString().replace /\D/g, ''
    event_start = if item.event_start then moment(item.event_start, 'YYYY/MM/DD') else null
    event_end = if item.event_end then moment(item.event_end, 'YYYY/MM/DD') else null
    is_event = Boolean(discount_price) and (event_start and event_end) and moment().isSameOrBefore(event_end, 'day')
    _.assign _.pick(item, item.keys or ['name', 'service_name', 'duration', 'description', 'discount_type']),
      service_category_id: item.category_id
      original_price: item.original_price?.replace(/\D/g, '') or null
      discount_rate: if item.discount_type is 'rate' then discount_price or 0
      discount_amount: if item.discount_type is 'amount' then discount_price or 0
      is_event: is_event
      event_start_ts: event_start?.startOf('day').format 'X'
      event_start_at: event_start?.startOf('day').format HB.time_format
      event_end_ts: event_end?.endOf('day').format 'X'
      event_end_at: event_end?.endOf('day').format HB.time_format

  $scope.service_categories = _.clone own.service_categories
  service_category_ids = _.map own.service_categories, 'id'

  do getServices = ->
    api.getServices
      params:
        shop_id: own.id
      success: (res) ->
        return if not data = res.data
        modelize item for item in data
        $scope.services = _.groupBy data, (item) -> if ~service_category_ids.indexOf(item.category_id) then item.category_id else null
      
  $scope.updateOrder = (ids) ->
    api.updateServiceOrder
      params:
        shop_id: own.id
      data:
        ordered_id_list: ids
      error: callback: -> getServices()
      success: (res) ->
        return if not data = res.data
        modelize item for item in data
        $scope.services = _.groupBy data, 'category_id'
        notify.success '저장되었습니다.'

  $scope.edit =
    textarea: false
    isEdit: false
    item: {}
    
    new: ->
      @isEdit = true
      @isNew = true
      @item =
        category_id: null
        category_name: '미분류'
        for_sale: true
        discount_type: 'rate'
        discount_price: 0

    enter: (id) ->
      @isEdit = true
      api.getService
        params:
          shop_id: own.id
          service_id: id
        success: (res) =>
          @item = modelize res.data

    updateName: ->
      @item.category_name = _.find($scope.service_categories, id: @item.category_id).name
      @item.name = if @item.category_id then "[#{@item.category_name}]#{@item.service_name}" else @item.service_name

    updateDiscount: ->
      if not /[1-9][\d,]*/.test @item.discount_price = @item.discount_price.toString().replace /^0+/, ''
        _.assign @item,
          discount_price: 0
          event_start: ''
          event_end: ''

    save: ->
      @updateName()
      if message = validator.service @item
        notify.error message
        return

      (if @isNew then api.createService else api.updateService)(
        params:
          shop_id: own.id
          service_id: @item.id
        data: normalize @item
        success: =>
          getServices()
          notify.success '저장되었습니다.'
          @exit()
      )

    delete: ->
      api.deleteService
        params:
          shop_id: own.id
          service_id: @item.id
        success: =>
          getServices()
          notify.success '삭제되었습니다.'
          @exit()

    exit: ->
      @isEdit = false
      @isNew = false
      @item = {}

  $scope.$on 'keydown', (e, args) ->
    return if not $scope.edit.isEdit
    switch args.code
      when 27 # esc
        $scope.edit.exit()
      when 13 # enter
        $scope.edit.save() if not $scope.edit.textarea