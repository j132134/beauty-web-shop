app = require '../app'

parser = require '../utility/parser'

app.filter 'phone', ->
  return (string) -> parser.phone string or ''
