app = require '../app'

moment = require 'moment'

app.filter 'fromnow', ->
  return (time) ->
    return if not time
    _time = moment time
    return if moment.isMoment(_time) then _time.fromNow() else time