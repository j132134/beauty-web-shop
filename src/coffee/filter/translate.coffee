app = require '../app'

korean = require '../locale/korean'

app.filter 'translate', ($rootScope) ->
  switch $rootScope.locale
    when 'ko' then return (string) -> korean[string] or string
    else return (string) -> string
