window.HB = {}

userAgent = navigator.userAgent

uaRegx =
  ie: /(MSIE) ([0-9]+\.?[0-9]*)/i
  ios: /(iPhone|iPad|iPod).*OS ([0-9_]+)/i
  android: /(Android) ([0-9.]+)/i

HB.ua = ua =
  ie: uaRegx.ie.exec userAgent
  ios: uaRegx.ios.exec userAgent
  android: uaRegx.android.exec userAgent

HB.platform = if ua.android then 'android' else if ua.ios then 'ios' else 'others'

HB.time_format = 'YYYY-MM-DD[T]HH:mm:ss.SSSZ'

## set moment locale
moment = require 'moment'
require 'moment-ko'
moment.updateLocale 'ko', relativeTime:
  future: '%s 후', past: '%s 전', s: '%d초', m: '1분', mm: '%d분', h: '1시간', hh: '%d시간', d: '하루', dd: '%d일', M: '한달', MM: '%d달', y: '1년', yy: '%d년'
moment.relativeTimeThreshold 's', 55
moment.relativeTimeThreshold 'm', 55
moment.relativeTimeThreshold 'h', 23
moment.relativeTimeThreshold 'd', 29
moment.relativeTimeThreshold 'M', 11

## angular bootstrapping
app = require './app'
require './routes'

app.init()
