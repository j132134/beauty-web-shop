require 'angular'
require 'angular-ui-router'

app = require './app'
states = require './states'

# load controllers
requireDir = (context) -> context.keys().forEach context
requireDir require.context './controller/', false, /\.coffee$/

# for resolve
require './service/session'
require './service/shop'

# routes config
app.config ($urlRouterProvider, $locationProvider, stateHelperProvider) ->

  $urlRouterProvider
  .when '', '/'
  .rule ($injector, $location) ->
    path = $location.path()
    path = path.replace /\/$/, '' if path.length > 1 and path[path.length - 1] is '/'
  .otherwise ($injector, $location) ->
    state = $injector.get '$state'
    state.go 'not-found'
    return $location.path()

  $locationProvider
  .html5Mode true
  .hashPrefix '!'

  _.each states, (state) ->
    stateHelperProvider.state state

# redirect to default child state instead of abstract
app.run ($rootScope, $state) ->
  $rootScope.$on '$stateChangeStart', (event, toState, params) ->
    if redirect = toState.redirect
      event.preventDefault()
      $state.go toState.name.concat(redirect), params
