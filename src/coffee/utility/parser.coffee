unicode = [
  [0x3131, 0x3132, 0x3134, 0x3137, 0x3138, 0x3139, 0x3141, 0x3142, 0x3143, 0x3145, 0x3146, 0x3147, 0x3148, 0x3149, 0x314a, 0x314b, 0x314c, 0x314d, 0x314e]
  [0x314f, 0x3150, 0x3151, 0x3152, 0x3153, 0x3154, 0x3155, 0x3156, 0x3157, 0x3158, 0x3159, 0x315a, 0x315b, 0x315c, 0x315d, 0x315e, 0x315f, 0x3160, 0x3161, 0x3162, 0x3163]
  [0x0000, 0x3131, 0x3132, 0x3133, 0x3134, 0x3135, 0x3136, 0x3137, 0x3139, 0x313a, 0x313b, 0x313c, 0x313d, 0x313e, 0x313f, 0x3140, 0x3141, 0x3142, 0x3144, 0x3145, 0x3146, 0x3147, 0x3148, 0x314a, 0x314b, 0x314c, 0x314d, 0x314e]
]

module.exports =

  number: (num) ->
    return num.toString()
    .replace /\D/g, ''
    .replace /\B(?=(\d{3})+(?!\d))/g, ','

  phone: (value, old = null, position = undefined) ->
    if not position or position is value?.length
      return value
      .replace /\D/g, ''
      .replace /(02|.{3})(.{1,8}).*/, '$1-$2'
      .replace /(.+)-((.{1,3})?(.{0,4})|(.{4})?(.{4}))$/, '$1-$3-$4-$5-$6'
      .replace /-+/g, '-'
      .replace /-$/, ''
    else
      if value?.length > old?.length
        index = _
          .chain old
          .slice 0, position - 1
          .filter (char) -> char is '-'
          .compact()
          .size()
          .value()
        count = old?.split('-')[index]?.length
        return {string: old, skip: true} if index > 0 and count is 4 or index is 0 and count is 3
      else if old[position] is '-'
        return {string: old, skip: true}
      else
        return null

  date: (value, old = null, position = undefined) ->
    if not position or position is value?.length
      return value
      .replace /\D/g, ''
      .replace /^(.{4})(.{0,2})(.{0,2}).*/, '$1/$2/$3'
      .replace /\/+/g, '/'
      .replace /\/$/, ''
    else
      if value?.length > old?.length
        index = _
        .chain old
        .slice 0, position - 1
        .filter (char) -> char is '/'
        .compact()
        .size()
        .value()
        count = old?.split('/')[index]?.length
        return {string: old, skip: true} if index > 0 and count is 2 or index is 0 and count is 4
      else if old[position] is '-'
        return {string: old, skip: true}
      else
        return null

  time: (time) ->
    time.replace /(\d{2})(\d{2})/, '$1시 $2분'

  korean: (string) ->
    result = ''
    _.each string, (char) =>
      code = char.charCodeAt(0) - 44032
      result += if 0 <= code <= 11171 then @syllable(code) else char.toLowerCase()
    return result

  syllable: (code) ->
    unit = [code / 588, code % 588 / 28, code % 588 % 28]
    result = String.fromCharCode(unicode[0][parseInt unit[0]])
    switch parseInt unit[1]
      when 9 then result += 'ㅗㅏ'
      when 10 then result += 'ㅗㅐ'
      when 11 then result += 'ㅗㅣ'
      when 15 then result += 'ㅜㅔ'
      when 14 then result += 'ㅜㅓ'
      when 16 then result += 'ㅜㅣ'
      when 19 then result += 'ㅡㅣ'
      else result += String.fromCharCode unicode[1][parseInt unit[1]]
    return result if not unit[2]
    switch parseInt unit[2]
      when 3 then result += 'ㄱㅅ'
      when 5 then result += 'ㄴㅈ'
      when 6 then result += 'ㄴㅎ'
      when 9 then result += 'ㄹㄱ'
      when 10 then result += 'ㄹㅁ'
      when 11 then result += 'ㄹㅂ'
      when 12 then result += 'ㄹㅅ'
      when 13 then result += 'ㄹㅌ'
      when 14 then result += 'ㄹㅍ'
      when 15 then result += 'ㄹㅎ'
      when 18 then result += 'ㅂㅅ'
      else result += String.fromCharCode unicode[2][parseInt unit[2]]
    return result
