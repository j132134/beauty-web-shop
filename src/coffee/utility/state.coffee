find = (states, name) ->
  array = name.split '.'
  do findChildren = (list = states, key = array[i = 0]) ->
    result = _.find list, name: key
    if result?.title
      return result.title
    else if _.isArray result?.children
      findChildren result.children, "#{key}.#{array[++i]}"

module.exports =
  getTitle: (name) ->
    find require('../states'), name
