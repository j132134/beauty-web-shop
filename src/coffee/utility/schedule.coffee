time = require './time'

module.exports =

  modelize: (schedules) ->
    return [] if _.isEmpty schedules
    _.chain schedules
    .groupBy (schedule) -> schedule.start + schedule.end
    .mapValues (days) ->
      [
        _.some days, day: 0
        _.some days, day: 1
        _.some days, day: 2
        _.some days, day: 3
        _.some days, day: 4
        _.some days, day: 5
        _.some days, day: 6
      ]
    .transform (result, value, key) ->
      result.push
        days: value
        start: key.slice 0, 4
        end: key.slice 4
    , []
    .sortBy ['start', 'end']
    .value()

  normalize: (schedules) ->
    return [] if _.isEmpty schedules
    _.chain schedules
    .map (schedule) ->
      _.transform _.invertBy(schedule.days).true, (result, day) ->
        result.push
          day: Number day
          start: _start = schedule.start.replace /\D/g, ''
          end: _end = schedule.end.replace /\D/g, ''
          startIndex: time.toIndex _start
          endIndex: time.toIndex _end
      , []
    .flatten()
    .value()

  convert: (data, weekday) ->
    if _.isUndefined weekday
      _data = _.chain(data).sortBy('start').groupBy('day').value()
      _.range(7).map (n, i) ->
        array = _.flatMap _data[i], (item) -> [time.toIndex(item.start), time.toIndex(item.end)]
        array.unshift 0
        array.push 24
        _.chunk array, 2
    else
      _.map data, (employment) ->
        _data = _.chain(employment.data).filter(day: weekday).sortBy('start').value()
        array = _.flatMap _data, (item) -> [time.toIndex(item.start), time.toIndex(item.end)]
        array.unshift 0
        array.push 24
        _.chunk array, 2
