module.exports =

  parse: (number) ->
    hour: parseInt number
    minute: if number % 1 then 30 else 0

  toString: (index, format) ->
    _hour = parseInt index
    _minutes = parseInt index % 1 / 0.0166
    hour = _.padStart _hour, 2, '0'
    minutes = _.padStart _minutes, 2, '0'
    switch format
      when 'HHmm' then "#{hour}#{minutes}"
      when 'H:mm' then "#{_hour}:#{minutes}"
      else "#{hour}:#{minutes}"

  toIndex: (string) ->
    return 0 if string.length isnt 4
    hour = Number string.slice 0, 2
    minute = Number string.slice 2
    return hour + if minute > 30 then 1 else if minute then .5 else 0

  toPoint: (string) ->
    return 0 if string.length isnt 4
    hour = Number string.slice 0, 2
    minute = Number string.slice 2
    return hour + minute / 60