module.exports =

  isNull: _isNull = (obj) ->
    if _.isBoolean obj
      return false
    else if _.isArray obj
      return not obj.length
    else if _.isObject obj
      return true if _.keys(obj).length is 0
      return Boolean _.countBy(obj, _isNull).true
    else
      return _.isNil(obj) or obj.length is 0

  shopInfo: (form, list) ->
    for vendor, regex of list
      if not (new RegExp regex).test form[vendor]
        form[vendor] = null
    return form

  phone: (number) ->
    number = number.replace /\D/g, ''
    return (/[0-9]{10,12}/).test number

  reservation: (item) ->
    if not item.service_id
      return '서비스를 선택해주세요.'
    else if not item.start_ts
      return '서비스 시간을 선택해주세요.'
    else
      return null

  service: (item) ->
    if not item.service_name
      return '서비스 이름을 입력해주세요.'
    else if not item.duration
      return '서비스 시간을 입력해주세요.'
    else if not item.original_price
      return '서비스 가격을 입력해주세요.'
    else if item.discount_price and not(item.event_start and item.event_end)
      return '이벤트 기간을 선택해주세요.'
    else
      return null

  employment: (item) ->
    if not item.employee_name
      return '이름을 입력해주세요.'
    else if not item.phone_num
      return '연락처를 입력해주세요.'
    return null