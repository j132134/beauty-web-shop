module.exports =

  concat: (object, array) ->
    if not object then return array
    return _.concat object, _.reject array, object
