module.exports = [
  {
    name: 'shop'
    abstract: true
    template: '<ui-view></ui-view>'
    controller: 'shopController'
    resolve:
      user: ['session', (session) ->
        session.validate()
      ]
      own: ['user', 'shop', (user, shop) ->
        shop.getInfo user
      ]
    children: [
      {
        name: 'dashboard'
        title: '대시보드'
        url: '/'
        templateUrl: '/template/dashboard.html'
        controller: 'dashboardController'
      }
      {
        name: 'customer'
        title: '고객 관리'
        url: '/customer'
        templateUrl: '/template/indevelop.html'
        onEnter: ['$rootScope', '$state', ($rootScope, $state) ->
          if $rootScope.unmanaged then $state.go 'shop.setting'
        ]
      }
      {
        name: 'reservation'
        title: '예약 관리'
        url: '/reservation?employee&date'
        templateUrl: '/template/reservation.html'
        controller: 'reservationController'
        onEnter: ['$rootScope', '$state', ($rootScope, $state) ->
          if $rootScope.disuse then $state.go 'shop.setting'
        ]
      }
      {
        name: 'employee'
        title: '직원 관리'
        url: '/employee/{id:int}'
        templateUrl: '/template/employee.html'
        controller: 'employeeController'
        onEnter: ['$rootScope', '$state', ($rootScope, $state) ->
          if $rootScope.unmanaged then $state.go 'shop.setting'
        ]
        resolve:
          employments: ['own', 'apiCache', (own, apiCache) ->
            apiCache.getEmployments shop_id: own.id
          ]
      }
      {
        name: 'service'
        title: '서비스 관리'
        url: '/service'
        templateUrl: '/template/service.html'
        controller: 'serviceController'
      }
      {
        name: 'setting'
        title: '샵 정보 설정'
        redirect: '.info'
        children: [
          {
            name: 'info'
            url: '^/setting'
            views: '@shop':
              templateUrl: '/template/setting.info.html'
              controller: 'settingController'
            resolve:
              meta: ['apiCache', (apiCache) ->
                apiCache.getMeta()
              ]
          }
          {
            name: 'portfolio'
            url: '^/portfolio'
            views: '@shop':
              templateUrl: '/template/setting.portfolio.html'
              controller: 'portfolioController'
          }
          {
            name: 'post'
            url: '^/post'
            views: '@shop':
              templateUrl: '/template/setting.post.html'
              controller: 'postController'
            onEnter: ['$rootScope', '$window', ($rootScope, $window) ->
              if not $rootScope.auth.is_peon then $window.history.back()
            ]
          }
        ]
      }
      {
        name: 'shops'
        title: '샵 리스트'
        url: '/shops/:manage'
        params: manage: 'manage'
        templateUrl: '/template/shops.html'
        controller: 'shopsController'
        onEnter: ['$rootScope', '$window', ($rootScope, $window) ->
          if not $rootScope.auth.is_peon then $window.history.back()
        ]
      }
      {
        name: 'switch'
        url: '/shop/{id:int}?redirect'
        controller: 'switchController'
      }
    ]
  }
  {
    name: 'account'
    children: [
      {
        name: 'signin'
        url: '/signin'
        views: '@':
          templateUrl: '/template/account/signin.html'
          controller: 'signInController'
        resolve:
          user: ['session', (session) ->
            session.validate(required: false)
          ]
        onEnter: ['$rootScope', '$state', 'notify', ($rootScope, $state, notify) ->
          if $rootScope.auth
            notify.info '이미 로그인 되어있습니다.'
            $state.go 'shop.dashboard', null, {location: 'replace', reload: true}
        ]
      }
      {
        name: 'signup'
        url: '/new'
        views: '@':
          templateUrl: '/template/account/signup.html'
          controller: 'signUpController'
        resolve:
          user: ['session', (session) ->
            session.validate(required: false)
          ]
          meta: ['apiCache', (apiCache) ->
            apiCache.getMeta()
          ]
      }
      {
        name: 'signout'
        url: '/signout'
        views: '@':
          controller: ['session', (session) ->
            session.destroy()
        ]
      }
    ]
  }
  {
    name: 'not-found'
    templateUrl: '/template/error.html'
    controller: ['$scope', '$window', ($scope, $window) ->
      $scope.type = 'not-found'
      $scope.back = -> $window.history.back()
    ]
  }
  {
    name: 'api-error'
    templateUrl: '/template/error.html'
    controller: ['$scope', '$window', ($scope, $window) ->
      $scope.type = 'api-error'
      $scope.reload = -> $window.location.reload()
    ]
  }
]
