require 'coffee-script'

path = require 'path'
del = require 'del'
gulp = require 'gulp'
jade = require 'gulp-jade'
stylus = require 'gulp-stylus'
base64 = require 'gulp-css-base64'
webpack = require 'webpack-stream'
sourcemaps = require 'gulp-sourcemaps'
coffeelint = require 'gulp-coffeelint'
stylint = require 'gulp-stylint'
rename = require 'gulp-rename'
replace = require 'gulp-replace'
connect = require 'gulp-connect'

## variables
config =
  gulp: require './gulp.config'
  webpack: require './webpack.config'

option = config.gulp.option
src = config.gulp.path.src
dist = config.gulp.path.dist

hash = undefined

error = ->
  @emit 'end'

## private tasks
gulp.task 'clean', ->
  del dist.root

gulp.task 'copy', ->
  gulp.src path.join src.image, '**/*'
  .pipe gulp.dest dist.img
  gulp.src path.join src.assets, '*.json'
  .pipe gulp.dest dist.root

# build
gulp.task 'jade:dev', ->
  gulp.src path.join src.jade, '**/*.jade'
  .pipe jade option.jade.dev
  .on 'error', error
  .pipe gulp.dest dist.root
  .pipe connect.reload()

gulp.task 'jade', ->
  gulp.src path.join src.jade, '**/*.jade'
  .pipe jade option.jade.prod
  .pipe gulp.dest dist.root

gulp.task 'stylus:dev', ->
  gulp.src path.join src.stylus, 'style.styl'
  .pipe sourcemaps.init()
  .pipe stylus option.stylus.dev
  .on 'error', error
  .pipe base64 option.base64
  .pipe sourcemaps.write '.'
  .pipe gulp.dest dist.css
  .pipe connect.reload()

gulp.task 'stylus', ->
  gulp.src path.join src.stylus, 'style.styl'
  .pipe stylus option.stylus.prod
  .pipe base64 option.base64
  .pipe gulp.dest dist.css

gulp.task 'webpack:dev', ->
  gulp.src path.join src.coffee, 'entry.coffee'
  .pipe webpack config.webpack.dev
  .on 'error', error
  .pipe gulp.dest dist.js
  .pipe connect.reload()

gulp.task 'webpack', ->
  gulp.src path.join src.coffee, 'entry.coffee'
  .pipe webpack config.webpack.prod
  .pipe gulp.dest dist.js

gulp.task 'lint:coffee', ->
  gulp.src [
    path.join __dirname, '*.coffee'
    path.join src.coffee, '**/*.coffee'
  ]
  .pipe coffeelint option.coffeelint
  .pipe coffeelint.reporter()

gulp.task 'lint:stylus', ->
  gulp.src path.join src.stylus, '**/*.styl'
  .pipe stylint option.stylint
  .pipe stylint.reporter()

# serve
gulp.task 'watch', ['copy', 'jade:dev', 'stylus:dev', 'webpack:dev'], ->
  gulp.watch 'assets/**/*', ['copy', 'stylus:dev']
  gulp.watch '**/*.jade', {cwd: src.jade}, ['jade:dev']
  gulp.watch '**/*.styl', {cwd: src.stylus}, ['stylus:dev']
  gulp.watch '**/*.coffee', {cwd: src.coffee}, ['webpack:dev']

gulp.task 'connect', ->
  connect.server
    root: dist.root
    port: 3000
    livereload: true
    fallback: path.join dist.root, config.gulp.index

# build
gulp.task 'replace:html', ['rename'], ->
  gulp.src [
    path.join dist.root, '*.html'
    path.join dist.root, '*.json'
  ]
  .pipe replace /\/(style|script)\.(css|js)/g, "/$1.#{hash}.$2"
  .pipe replace '_hash_', hash
  .pipe gulp.dest dist.root

gulp.task 'replace:js', ['webpack'], ->
  gulp.src path.join dist.js, '*.js'
  .pipe replace /(\/(?:template|components)\/[^.]+)\.(html)/g, "$1.#{hash}.$2"
  .pipe gulp.dest dist.js

gulp.task 'rename', ['jade', 'stylus', 'webpack'], ->
  gulp.src [
    path.join dist.root, 'components/**/*.html'
    path.join dist.root, 'template/**/*.html'
    path.join dist.css, '*.css'
    path.join dist.js, '*.js'
  ]
  .pipe rename (path) ->
    path.basename += ".#{hash}"
  .pipe gulp.dest (file) ->
    return file.base

## public tasks
gulp.task 'default', ->
  console.log 'Run "gulp serve" or "gulp build"'

gulp.task 'lint', ->
  gulp.start ['lint:coffee', 'lint:stylus']

gulp.task 'serve', ['clean'], ->
  gulp.start ['copy', 'jade:dev', 'stylus:dev', 'webpack:dev', 'watch', 'connect']

gulp.task 'build', ['clean'], ->
  hash = (new Date).getTime()
  gulp.start ['copy', 'jade', 'stylus', 'webpack', 'replace:html', 'replace:js', 'rename']
