_ = require 'lodash'
path = require 'path'
webpack = require 'webpack'
BowerWebpackPlugin = require 'bower-webpack-plugin'
ngAnnotatePlugin = require 'ng-annotate-webpack-plugin'

base =
  output:
    filename: 'script.js'

  module:
    loaders: [
      {test: /\.coffee$/, loader: 'coffee-loader'}
    ]

  resolve:
    root: path.resolve './src/coffee'
    extensions: ['', '.coffee', '.js']
    alias:
      'lodash': path.join __dirname, 'bower_components/lodash/lodash.js'
      'moment-ko': path.join __dirname, 'bower_components/moment/locale/ko.js'

plugins =
  banner: [
    new webpack.BannerPlugin('"use strict";', {
      raw: true
    })
  ]
  shim: [
    new webpack.ProvidePlugin({
      _: 'lodash'
      $: 'jquery'
      jQuery: 'jquery'
      'window.jQuery': 'jquery'
    })
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ko/)
    new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/])
  ]
  bower: [
    new BowerWebpackPlugin({
      modulesDirectories: ['bower_components']
      manifestFiles: 'bower.json'
      includes: /.*/
      excludes: []
      searchResolveModulesDirectories: false
    })
  ]
  annotate: [
    new ngAnnotatePlugin({
      add: true
    })
  ]
  uglify: [
    new webpack.optimize.UglifyJsPlugin({
      minimize: true
      sourceMap: false
      output:
        comments: false
      compress:
        drop_debugger: false
        hoist_funs: true
        hoist_vars: true
        cascade: false
    })
  ]

module.exports =
  dev: _.assign
    cache: false
    debug: true
    devtool: '#source-map'
    plugins: _.concat plugins.banner, plugins.shim, plugins.bower
  , base

  prod: _.assign
    cache: false
    debug: false
    plugins: _.concat plugins.banner, plugins.shim, plugins.bower, plugins.annotate, plugins.uglify
  , base
