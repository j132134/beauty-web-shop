path = require 'path'

assets = path.join __dirname, 'assets/'
src = path.join __dirname, 'src/'
dist = path.join __dirname, 'dist/'

module.exports =
  path:
    src:
      jade: path.join src, 'jade/'
      stylus: path.join src, 'stylus/'
      coffee: path.join src, 'coffee/'
      image: path.join assets, 'image/'
      assets: assets
    dist:
      root: dist
      css: path.join dist, 'css/'
      js: path.join dist, 'js/'
      img: path.join dist, 'img/'

  index: 'index.html'

  option:
    jade:
      dev:
        pretty: true
      prod:
        pretty: false

    stylus:
      dev:
        compress: false
      prod:
        compress: true

    base64:
      baseDir: __dirname
      maxWeightResource: 1000000
      extensionsAllowed: ['.woff']

    coffeelint:
      arrow_spacing:
        level: 'error'
      braces_spacing:
        spaces: 0
        empty_object_spaces: 0
        level: 'error'
      camel_case_classes:
        level: 'ignore'
      colon_assignment_spacing:
        spacing:
          left: 0
          right: 1
        level: 'error'
      eol_last:
        level: 'warn'
      indentation:
        value: 2
        level: 'error'
      line_endings:
        level: 'error'
      max_line_length:
        level: 'ignore'
      missing_fat_arrows:
        level: 'ignore'
      newlines_after_classes:
        level: 'error'
      no_empty_functions:
        level: 'error'
      no_empty_param_list:
        level: 'error'
      no_implicit_braces:
        level: 'ignore'
      no_implicit_parens:
        level: 'ignore'
      no_interpolation_in_single_quotes:
        level: 'warn'
      no_stand_alone_at:
        level: 'warn'
      no_tabs:
        level: 'error'
      no_this:
        level: 'error'
      no_unnecessary_double_quotes:
        level: 'warn'
      non_empty_constructor_needs_parens:
        level: 'warn'
      prefer_english_operator:
        level: 'error'
      space_operators:
        level: 'error'
      spacing_after_comma:
        level: 'error'

    stylint:
      rules:
        blocks: false
        brackets: 'never'
        colons: 'never'
        colors: 'always'
        commaSpace: 'always'
        commentSpace: 'always'
        cssLiteral: 'never'
        depthLimit: false
        duplicates: false
        efficient: 'always'
        exclude: []
        extendPref: false
        globalDupe: true
        indentPref: 2
        leadingZero: 'never'
        maxErrors: false
        maxWarnings: false
        mixed: false
        namingConvention: 'lowercase-dash'
        namingConventionStrict: false
        none: 'never'
        noImportant: false
        parenSpace: 'never'
        placeholders: 'always'
        prefixVarsWithDollar: 'never'
        quotePref: false
        semicolons: 'never'
        sortOrder: false
        stackedProperties: 'never'
        trailingWhitespace: 'never'
        universal: 'never'
        valid: false
        zeroUnits: 'never'
        zIndexNormalize: false
